<?php
include("lib/materialize.php");
include("Controller/api.php");
date_default_timezone_set("Asia/Singapore");

if($_SESSION['role']=="client"){
  echo header("location:client.php");
}

if(!isset($_SESSION['email'])){
  echo header("location:index.php");
}
else{
  $page=$_SERVER['PHP_SELF'];
  $sec="300";
  $current_time=date('Y-m-d H:i:s');
  Download_PatrolData();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'">
  <link rel="icon" href="image/logoicon.png">
  <?php include("lib/react.php"); ?>
  <title>Clocking Dashboard</title>
</head>
<body>

 	<div class="row">
	 	<?php include("lib/nav.php"); ?>
    <div class="progress">
      <div class="indeterminate"></div>
    </div>
 	</div>

  <div class="row">
    <h2 class="center mss">Clocking Status <span class="white-text" id="time"> </span></h2>
  </div>

  <div class="row">
    <div class="col s12 m12 l12">

       <div class="row" id="important">
          <h5 class="center">1st Level *IMPORTANT*</h5>
       </div>

       
         <?php include('View/Important_Clock.php'); ?>       

    </div>
  </div>

<!-- //////////////////////////////////////////////////////  Cluster ////////////////////////////////////////////// -->
  <div class="row">
    
  </div>

 	<div class="row">
    <div class="col s12 m12 l12">
        <div class="row" id="important">
          <h5 class="center">2nd Level *IMPORTANT*</h5>
       </div>
    </div>

 		<div class="col s12 m12 l3">
 			 <div class="row" id="east">
          <h5 class="center">EAST</h5>
			 </div>
       <div class="row">
         <?php include('View/East_Clock.php'); ?>
       </div>
 		</div>

    <div class="col s12 m12 l3">
 			 <div class="row" id="west">
          <h5 class="center">WEST</h5>
			 </div>
       <div class="row">
         <?php include('View/West_Clock.php'); ?>
       </div>
 		</div>

    <div class="col s12 m12 l3">
 			 <div class="row" id="north">
          <h5 class="center">NORTH</h5>
			 </div>
       <div class="row">
         <?php include('View/North_Clock.php'); ?>
       </div>
 		</div>

    <div class="col s12 m12 l3">
 			 <div class="row" id="central">
          <h5 class="center">CENTRAL</h5>
			  </div>
        <div class="row">
          <?php include('View/Central_Clock.php'); ?>
        </div>
 		</div>
  </div>

<?php include("lib/js.php"); ?>
<?php include("materialize/app.js"); ?>



</body>
</html>
<?php  } ?>
