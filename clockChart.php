<?php
include("Controller/api.php");
include("Controller/db.php");
date_default_timezone_set("Asia/Singapore");
  $todayDate=date('Y-m-d');
  $param_time1=date('Y-m-d 00:00:00');
  $param_time2=date('Y-m-d 23:59:59');
  
  ////////////////////////////// Bar Chart Data ////////////////////////////
  $data1=array();
  for($i=1;$i<=7;$i++){
    $data1[]=clock_data2($_SESSION['siteid'],date('Y-m-d 00:00:00',strtotime($param_time1.'-'.$i.'day')),date('Y-m-d 23:59:59',strtotime($param_time2.'-'.$i.'day')),'report_id','server_received_date');  
  }
  $day1=sizeof($data1[0]);
  $day2=sizeof($data1[1]);
  $day3=sizeof($data1[2]);
  $day4=sizeof($data1[3]);
  $day5=sizeof($data1[4]);
  $day6=sizeof($data1[5]);
  $day7=sizeof($data1[6]);

  ////////////////////////////// Line Chart Data ///////////////////////////
  $data2=array();
  $mktime=array();

  for($i=1;$i<=7;$i++){
    $data2[]=clock_data2($_SESSION['siteid'],date('Y-m-d 00:00:00',strtotime($param_time1.'-'.$i.'day')),date('Y-m-d 23:59:59',strtotime($param_time2.'-'.$i.'day')),'start_clock_time','end_clock_time');
  }
  for($ii=0;$ii<sizeof($data2);$ii++){
    for($iii=0;$iii<sizeof($data2[$ii]);$iii++){
      $mktime[]=ceil((strtotime($data2[$ii][$iii]['end_clock_time'])-strtotime($data2[$ii][$iii]['start_clock_time']))/60);
    }    
    $dbdc[]=array_sum($mktime)/sizeof($mktime);
    unset($mktime);
  }
  /////////////////////////////// Radial for Avg Time Spent ///////////////////

  $data4[]=clock_data2($_SESSION['siteid'],$param_time1,$param_time2,'start_clock_time','end_clock_time');
  for($i=0;$i<sizeof($data4[0]);$i++){
    $mktime2[]=ceil((strtotime($data4[0][$i]['end_clock_time'])-strtotime($data4[0][$i]['start_clock_time']))/60);
  }
  $data5[]=clock_data2($_SESSION['siteid'],date('Y-m-d 00:00:00',strtotime("first day of last month")),date('Y-m-d 23:59:59',strtotime("last day of last month")),'start_clock_time','end_clock_time');
  //print_r($data5);
  for($i=0;$i<sizeof($data5[0]);$i++){
    $mktime3[]=ceil((strtotime($data5[0][$i]['end_clock_time'])-strtotime($data5[0][$i]['start_clock_time']))/60);
  }
  $mthAvgTime=round((array_sum($mktime3)/sizeof($mktime3)),2);
  $tdAvgTime=round((array_sum($mktime2)/sizeof($mktime2)),2);

  
  
  //////////////////////////////// Pie Chart Data /////////////////////////////
  for($j=1;$j<3;$j++){
    if($j==1){
      $data3_1[]=clock_data2($_SESSION['siteid'],date('Y-m-d 00:00:00',strtotime("first day of last month")),date('Y-m-d 23:59:59',strtotime("last day of previous month")),'report_id','start_clock_time');
    }
    else if($j==2){
      $data3_2[]=clock_data2($_SESSION['siteid'],date('Y-m-d 00:00:00',strtotime("first day of last month")),date('Y-m-d 23:59:59',strtotime("last day of previous month")),'report_id','start_clock_time');
    }
  }
  $data3[]=clock_data2($_SESSION['siteid'],date('Y-m-d 00:00:00',strtotime("first day of this month")),date('Y-m-d 23:59:59',strtotime("last day of this month")),'report_id','missed_checkpoint');

  for($i=0;$i<=sizeof($data3[0])-1;$i++){
    if($data3[0][$i]['missed_checkpoint']=="false"){
      $nomissed[]=$data3[0][$i]['missed_checkpoint'];
    }
    else{
      $missed[]=$data3[0][$i]['missed_checkpoint'];
    }
  }
 
  $percentComplete=round((sizeof($nomissed)*100)/(sizeof($nomissed)+sizeof($missed)),2);
  $percentIncomplete=round(100-$percentComplete,2);
  
  ///////////////////////////////// Table Result /////////////////////////////////
  $order="ORDER BY start_clock_time DESC";
  $tblData1=clock_data2($_SESSION['siteid'],date('Y-m-d 00:00:00',strtotime("-2 days")),date('Y-m-d 23:59:59',strtotime("yesterday")),'report_link','remarks','start_clock_time','end_clock_time','missed_checkpoint',$order);
  

?>
<!DOCTYPE html>
<html>
<head>
  <title>Clocking Chart</title>
  <?php include("lib/bootstrap.php"); ?>
  <?php include("lib/materialize.php"); ?>
  <?php include("lib/react.php"); ?>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.js"></script>
  <script src="https://cdn.jsdelivr.net/gh/emn178/chartjs-plugin-labels/src/chartjs-plugin-labels.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
  <script type='text/javascript'>
    $(document).ready(function(){
      $('#dateFrom,#dateTo').change(function(event){
        var dateFrom=$('#dateFrom').val();
        var dateTo=$('#dateTo').val();
        $.ajax({
          url:'ajaxTest.php',
          data:{"dateFrom":dateFrom,"dateTo":dateTo},
          success:function(data){
            $('#ajaxTable').html(data)
          }
        });
      });
      function ajaxLoader(){
        $.ajax({
            url:'ajaxTest.php',
            data:{"pstartTime":"<?= $param_time1 ?>","pendTime":"<?= $param_time2 ?>"},
            success:function(data){
              $('#missedAlert').html(data);
            }
        });
      }

      setInterval(function(){
        ajaxLoader();     
      },3000);
      
});
  </script>
</head>
<body id="bdid">
  <div class="row">
    <?php 
    if($_SESSION['role']=="client"){ 
      include("lib/nav-client.php");
    }
    else{
      include("lib/nav.php");
    }
    ?>    
    <nav class="w3-sidenav z-depth-5" style="width:15%">
      <a class="text-center" href="#"><img src="image/logo.png" width="150" height="90"/><h5>Metropolis Security Systems</h5></a> 
      <a class="w3-hover-blue" href="#">Home</a> 
      <a class="w3-hover-blue" href="#resultTable">Clocking Analysis System</a> 
    </nav>
  </div>
  <div class="container-fluid">
  
    <div class="row">
      <div class="col-md-12 col-lg-2"> </div>
      <div class="col-md-12 col-lg-10" id="missedAlert"> </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-lg-2"> </div>
        <div class="col-md-12 col-lg-10" id="header"> 
          <h2 class="text-center">Clocking Analysis System (CAS)</h2>
          <span class="text-center" id="time"> </span>
        </div>
    </div>

    <div class="row">
      <div class="col-lg-4"> </div>
      <div class="col-sm-12 col-md-3 col-lg-2">
          <div id="card"> </div>
        </div>
        <div class="col-sm-12 col-md-3 col-lg-2">
          <div id="card1"> </div>
        </div>
        <div class="col-sm-12 col-md-3 col-lg-2">
          <div id="card2"> </div>
        </div>
    </div>
    
    <div class="row">
      <div class="col-lg-3"> </div>
      <div class="col-sm-6 col-md-6 col-lg-4" id="chart1" style="position: relative">
        <canvas id="myBar" width="200" height="135"></canvas><br/>
      </div>
      <div class="col-sm-6 col-md-6 col-lg-4" id="chart2">
        <canvas id="myLine" width="200" height="135"></canvas>
      </div>        
    </div>

    <div class="row">
      <div class="col-sm-4 col-md-4 col-lg-3"> </div>
      <div class="col-sm-12 col-md-12 col-lg-4" id="chart3">
        <canvas id="myPie" width="200" height="135"></canvas><br/>
      </div>
      <div class="col-sm-12 col-md-12 col-lg-4" id="chart4">
        <canvas id="myPie2" width="200" height="135"></canvas><br/>
      </div>
        
      <!-- <table class="table-striped text-center">
            <thead>
              <form method="post" action="">
              <tr>
                <th class="text-center" colspan=4><h5>Clocking Insight</h5></th>
              </tr>
              <tr>
                <th class="text-center">Title 1</th>
                <th class="text-center">Title 2</th>
                <th class="text-center">Title 3</th>
                <th class="text-center">Title 4</th> 
              </tr>
            </thead>
          
            <tbody id="insight">
              
            </tbody>
          </tr>
        </table> -->
    </div>


    <div class="row">
      <div class="col-sm-1 col-md-2 col-lg-3"> </div>
      <div class="col-sm-10 col-md-10 col-lg-8" id="resultTable">   
        <table class="table-dark text-center">
            <thead>
              <form method="post" action="">
              <tr>
                <th class="text-center" colspan=5><h4>Filter Clocking Report</h4></th>
              </tr>
              <tr>
                <th class="text-center" colspan=5>
                <div class="input-field col s12 m12 l4 offset-l4">
                    <i class="material-icons prefix">access_alarm</i>
                    <input id="dateFrom" name="startdate" type="text" class="datepicker validate" autocomplete="off">
                    <label for="icon_prefix">Date From</label>
                </div>
                </th>
              </tr>
              <tr>
                <th class="text-center" colspan=5>
                <div class="input-field col s12 m12 l4 offset-l4">
                      <i class="material-icons prefix">access_alarm</i>
                      <input id="dateTo" name="enddate" type="text" class="datepicker validate" autocomplete="off">
                      <label for="icon_prefix">Date To</label>
                </div>
                </th>
              </tr>
              <tr>
                <th class="text-center" colspan=5>
                <button class="btn-large waves-effect waves-light" type="submit" onclick="window.refresh()" name="submit" id="filterDate">reset
                  <i class="material-icons right">refresh</i>
                </button>
                </th>
              </tr>
              </form>
              <tr>
                <th class="text-center">View Report</th>
                <th class="text-center">Time Start</th>
                <th class="text-center">Time End</th>
                <th class="text-center">Any Miss Points</th>
                <th class="text-center">Remark</th>
              </tr>
            </thead>
          
            <tbody id="ajaxTable">
              <?php
              for($i=0;$i<sizeof($tblData1);$i++){
                echo "<tr>";
                echo "<td class='text-center'><a href='".$tblData1[$i]['report_link']."' target='_blank' id='ajaxbtn'><i class='material-icons'>remove_red_eye</i></a></td>";
                echo "<td class='text-center'>".date('d-M H:i:s',strtotime($tblData1[$i]['start_clock_time']))."</td>";
                echo "<td class='text-center'>".date('d-M H:i:s',strtotime($tblData1[$i]['end_clock_time']))."</td>";
                if($tblData1[$i]['missed_checkpoint']=="true"){
                echo "<td class='text-center'>YES - <a href='".$tblData1[$i]['report_link']."' class='red-text' target='_blank' id='ajaxbtn'>View Report</a></td>";
                }
                else{
                  echo " ";
                }
                echo "<td class='text-center'>".$tblData1[$i]['remarks']."</td>";
                echo "</tr>";
              }
              ?>
            </tbody>
          </tr>
        </table>
      </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12 text-center">
              <p>Copyright © <?= date('Y') ?> Metropolis Security Systems Pte Ltd. All rights reserved</p>
        </div>
    </div>




  </div>
  <?php include("materialize/graph.js"); ?>
  <?php include("lib/bootstrap_js.php"); ?>
  <?php include("lib/js.php"); ?>
  <?php include("materialize/app.js"); ?>
</body>
</html>