<?php
include("lib/materialize.php");
include("Controller/api.php");
include("Controller/db.php");
date_default_timezone_set("Asia/Singapore");

if(!isset($_SESSION['email'])){
  echo header("location:index.php");
}
else{
  $page=$_SERVER['PHP_SELF'];
  $sec="1200";
  $current_time=date('Y-m-d H:i:s');
  Download_PatrolData();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'">
  <link rel="icon" href="image/logoicon.png">
  <title>Clocking Dashboard</title>
</head>
<body>

 	<div class="row">
	 	<?php include("lib/nav-client.php"); ?>
 	</div>

   <div class="row">
 		<?php include("View/clocking-report.php"); ?>
   </div>

 	<div class="row">
 		<?php include("View/clocking-table.php"); ?>
   </div>
  

<?php include("lib/js.php"); ?>



</body>
</html>
<?php  } ?>
