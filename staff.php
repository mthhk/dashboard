<?php
include("lib/materialize.php");
include("Controller/api.php");
include("Controller/db.php");
date_default_timezone_set("Asia/Singapore");

if($_SESSION['role']=="client"){
  echo header("location:client.php");
}

if(!isset($_SESSION['email'])){
  echo header("location:index.php");
}
else{
  $page=$_SERVER['PHP_SELF'];
  $sec="60";
  $current_time=date('Y-m-d H:i:s');
  $timing1=date('Y-m-d 04:00:00');
  $timing2=date('Y-m-d 16:59:59');
  $dump_time=date('Y-m-d 03:59:59');//Day

  $time1=date('Y-m-d 04:00:00');//Day
  $time2=date('Y-m-d 16:59:59');//Day

  $time3=date('Y-m-d 17:00:00');//Night
  $time4=date('Y-m-d 04:59:59');//Night

  $time5=date('Y-m-d 23:59:59', strtotime($ttime1 .'-1 day'));


?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'">
  <title>MSS Attendance</title>
  <script src="http://code.responsivevoice.org/responsivevoice.js"></script>
  <script>
      var text="<?php echo "Welcome to Metropolis Security Systems"; ?>";
      responsiveVoice.speak(text, "Fallback UK Female", {pitch: 1.0,rate:0.8});
  </script>

</head>
<body>

  <div class="row">
    <?php include("lib/nav.php"); ?>
    <?php include("test.php"); ?>
    <div class="progress">
      <div class="indeterminate"></div>
    </div>
  </div>

  <div class="row">
    <h3 class="center mss">Management Staff</h3>
  </div>
 

<!-- //////////////////////////////////////////////////////  Data  ////////////////////////////////////////////// -->
  <div class="row" id="all">

    <div class="col s12 m6 l8 offset-l2">
       <div class="row" id="dcm">
          <h5 class="center">HQ Office / Operations Office</h5>
       </div>
    </div>
  </div>

  <div class="row">
      <div class="col s12 m6 l2"> </div>
      <?php include('View/staff.php'); ?>
  </div>


<?php include("lib/js.php"); ?>
</body>
</html>
<?php  } ?>
