<?php
include("lib/materialize.php");
include("Controller/api.php");
include("Controller/db.php");
date_default_timezone_set("Asia/Singapore");

if($_SESSION['role']=="client"){
  echo header("location:client.php");
}

if(!isset($_SESSION['email'])){
  echo header("location:index.php");
}
else{
  $page=$_SERVER['PHP_SELF'];
  $sec="300";
  $current_time=date('Y-m-d H:i:s');
  $timing1=date('Y-m-d 04:00:00');
  $timing2=date('Y-m-d 16:59:59');
  $dump_time=date('Y-m-d 03:59:59');//Day

  $time1=date('Y-m-d 04:00:00');//Day
  $time2=date('Y-m-d 16:59:59');//Day

  $time3=date('Y-m-d 17:00:00');//Night
  $time4=date('Y-m-d 03:59:59');//Night

  $time5=date('Y-m-d 23:59:59', strtotime($ttime1 .'-1 day'));


?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'">
  <title>MSS Attendance</title>
</head>
<body>

 	<div class="row">
	 	<?php include("lib/nav.php"); ?>
    <?php	include("test.php"); ?>
    <div class="progress">
      <div class="indeterminate"></div>
    </div>
 	</div>

  <div class="row">
    <h3 class="center mss">Attendance Status</h3>
  </div>


  <?php include('View/wrongic.php'); ?>
  <?php include('View/ot.php'); ?>
  <?php include('View/otdata.php'); ?>


<!-- //////////////////////////////////////////////////////  Cluster ////////////////////////////////////////////// -->


 	<div class="row">


      <div class="row center">
        <div class="col s12 m12 l12">
        <?php
          if(!empty($west_wrong)){
        ?>
        <a href="#westwrongic" class="btn btn-floating btn-large orange darken-1 pulse modal-trigger"><i class="material-icons">priority_high</i></a>
        <?php
        }
        else{
        ?>
        <a href="#westwrongic" class="btn btn-floating btn-large orange darken-1 disabled"><i class="material-icons">priority_high</i></a>
        <?php
        }
        if($west==true){
        ?>
        <a href="#otwest" class="btn btn-floating btn-large light-blue accent-3 pulse modal-trigger"><i class="material-icons">access_alarm</i></a>
        <?php
        }
        else{
        ?>
        <a href="#otwest" class="btn btn-floating btn-large light-blue accent-3 disabled"><i class="material-icons">access_alarm</i></a>
        <?php } ?>

        <?php
        if($westn==true){
        ?>
        <a href="#westn" class="btn btn-floating btn-large teal lighten-1 pulse modal-trigger"><i class="material-icons">access_alarm</i></a>
        <?php } 
        else{
        ?>
        <a href="#westn" class="btn btn-floating btn-large light-blue accent-3 disabled"><i class="material-icons">access_alarm</i></a>
        <?php
        }
        ?>
        </div>
      </div>

 			<div class="row center" >
         <div class="col s12 m12 l4 offset-l4" id="west">
           <h5 class="center">WEST</h5>
			    </div>
       </div>

      <div class="row">
            <?php include('View/West.php'); ?>
        </div>


  </div>




<?php include("lib/js.php"); ?>
</body>
</html>
<?php  } ?>
