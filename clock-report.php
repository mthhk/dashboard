<?php
include("Controller/api.php");
include("Controller/db.php");
date_default_timezone_set("Asia/Singapore");

if(!isset($_SESSION['email'])){
	echo header("location:index.php");
}
else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>Clocking Report</title>
  <?php include("lib/materialize.php"); ?>

</head>
<body>

 	<div class="row">
	 	<?php 
	 	if($_SESSION['role']=="client"){
	 		include("lib/nav-client.php"); 
	 	}
	 	else{
	 		include("lib/nav.php"); 	
	 	}

	 	?>
 	</div>

 	<div class="row">
 		<?php include("View/clocking-report.php"); ?>
 	</div>

 	<div class="row">
 		<?php include("View/clocking-table.php"); ?>
 	</div>


<?php include("lib/js.php"); ?>
</body>
</html>
<?php } ?>
