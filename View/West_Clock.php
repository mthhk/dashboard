<?php

/////////////////////////////// DATA Retrieving /////////////////////////////////
$siteid=clock_site('site','site_cluster','West','site_id');
$sitename=clock_site('site','site_cluster','West','site_name');
$num_clock=clock_site('site','site_cluster','West','num_clocking');
$today_clock=clock_site('site','site_cluster','West','today_clock');
$timing=clock_site('site','site_cluster','West','timing');

for($i=0;$i<sizeof($siteid);$i++){
  $sid=$siteid[$i];

  $param_time1=date('Y-m-d 00:00:00');
  $param_time2=date('Y-m-d 23:59:59');

  $data=clock_data('c.cus_id='.$sid,$param_time1,$param_time2,'report_id');
  $report_link=clock_data('c.cus_id='.$sid,$param_time1,$param_time2,'report_link');
  $route=clock_data('c.cus_id='.$sid,$param_time1,$param_time2,'contract_title');
  $done_by=clock_data('c.cus_id='.$sid,$param_time1,$param_time2,'done_by');
  $start_at=clock_data('c.cus_id='.$sid,$param_time1,$param_time2,'start_clock_time');
  $end_at=clock_data('c.cus_id='.$sid,$param_time1,$param_time2,'end_clock_time');
  $remarks=clock_data('c.cus_id='.$sid,$param_time1,$param_time2,'remarks');
  $missed_points=clock_data('c.cus_id='.$sid,$param_time1,$param_time2,'missed_checkpoint');

  $completed_data=sizeof($data);

  $time_array=unserialize($timing[$i]);

  include 'condition.php';


?>
<div id="<?= $color ?>">
  <table>
    <tr>
        <td colspan="4" class="center" id="clock_sitename"><a href="#modalwest<?= $i ?>" class="modal-trigger tooltipped" data-position="left" data-html="true" data-tooltip="<?php echo "<h5>Clocking Hrs</h5>"; for($array=0;$array<sizeof($time_array);$array++){echo "<h6>".$time_array[$array]."</h6><br/>"; } ?>"><?= $sitename[$i] ?></a></td>
    </tr>
    <!-- fixed column -->
    <tr>
      <td class="center" id="timing"><a class="tooltipped" data-position="top" data-tooltip="Complete">[ C ]</a></td>
      <!-- <td class="center" id="timing"><a class="tooltipped" data-position="top" data-tooltip="Extra">[ E ]</a></td> -->
      <td class="center" id="timing"><a class="tooltipped" data-position="top" data-tooltip="Incomplete Route">[ M ]</a></td>
      <td class="center" id="timing"><a class="tooltipped" data-position="top" data-tooltip="Total">[ T ]</a></td>
    </tr>
    <!-- count number of time doing clocking -->
    <tr>
      <td class="center" id="timing"><a class="btn-floating waves-effect blue-grey darken-3"><?= $completed_data ?></a></td>
      <!-- <td class="center" id="timing"><a class="btn-floating waves-effect blue-grey darken-3"><?= $extra ?></a></td> -->
      <td class="center" id="timing"><a class="btn-floating waves-effect blue-grey darken-3 <?= $animation ?>"><?= sizeof($total_missed) ?></a></td>
      <td class="center" id="timing"><a class="btn-floating waves-effect blue-grey darken-3"><?= $num_clock[$i] ?></a></td>
    </tr>
  </table>
</div><br>


  <div id="modalwest<?= $i ?>" class="modal modal-fixed-footer">
      <div class="modal-content">
        <h4><?= $sitename[$i] ?> [Clocking Status]</h4>

        <div class="col s12 m12 l12">
          <table id="data" class="bordered highlight">
            <tr>
                <th class="center">Report Link</th>
                <th class="center">Route</th>
                <th class="center">Done By</th>
                <th class="center">Start At</th>
                <th class="center">End At</th>
                <th class="center">Remarks</th>
            </tr>
            <?php
            if(empty($data)){
            ?>
            <tr>
                <td class="center" colspan="6"><h4 class="red-text animated infinite fadeIn">Sorry! No Record Found</h4></td>
            </tr>
            <?php
            }
            for($j=0;$j<$completed_data;$j++){
            ?>
            <tr>
                <td class="center"><a href="<?= $report_link[$j] ?>" target="__Blank"> View Report </a></td>
                <td class="center"><?= $route[$j] ?></td>
                <td class="center"><?= $done_by[$j] ?></td>
                <td class="center"><?= $start_at[$j] ?></td>
                <td class="center"><?= $end_at[$j] ?></td>
                <td class="center"><?= $remarks[$j] ?></td>

            </tr>
            <?php } ?>
            </table>
      </div>

    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-green btn red darken-4">Close</a>
    </div>
    </div>
<?php
}
?>
