<div class="col s12 m12 l4"> </div>

<div class="col s12 m12 l4 center">

      <h3 align="center">Log In</h3>

      <form action="Controller/login-controller.php" method="post">

      <div class="row">
        <div class="input-field col s12">
          <input id="email" name="email" type="email" class="validate" required>
          <label for="email">Email</label>
        </div>
      </div>

      <div class="row">
        <div class="input-field col s12">
          <input id="password" name="password" type="password" class="validate" required>
          <label for="password">Password</label>
        </div>
      </div>

      <div class="row">
        <button id="btn" class="btn-large waves-effect waves-light" type="submit" name="action">Login<i class="material-icons right">https</i></button>
      </div>

    </div>
    </form>

<div class="col s12 m12 l4"> </div>
