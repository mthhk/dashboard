<?php
$filterdate=$_POST['datefilter'];
if(empty($filterdate)){
  $date=date('Y-m-d');
}
else{
  $date=$filterdate;
}
?>
<div class="row">
  <div class="col s12 m12 l2 offset-l10 right-align">
    <a href="Controller/export.php?date=<?php echo $date; ?>" class="btn-floating btn-large waves-effect waves-light tooltipped" data-position="top" data-tooltip="Export .csv" type="submit" name="download" id="downloadbtn">
      <i class="material-icons right">file_download</i>
    </a>
  </div>
</div>

<div class="row">
    <div class="col s12 m12 l12" >
        <table class="responsive-table highlight centered bordered striped hoverable sortable" id="mgmttable">
            <thead>
              <tr>
                  <th>Name</th>
                  <th>Check In</th>
                  <th>Check Out</th>
                  <th>Duration (Hour)</th>
                  <th>Site Code</th>
                  <th>Cluster</th>
              </tr>
            </thead>
            <tbody>

            <?php 
              if(empty($filterdate)){
              $time1=date('Y-m-d 00:00:00');
              $time2=date('Y-m-d 23:59:59');

              $data=OEMove($time1,$time2);
              for($row=0;$row<sizeof($data);$row++){
            ?>
            <tr>
                <?php
                for($col=0;$col<sizeof($data[0]);$col++){
                ?>
                  <td><?= $data[$row][$col]; ?></td>

                <?php } ?>
            </tr>            
            <?php }
            }else{
                $starttime=date('Y-m-d 00:00:00',strtotime($filterdate));
                $endtime=date('Y-m-d 23:59:59',strtotime($filterdate));
                
                $data=OEMove($starttime,$endtime);
                for($row=0;$row<sizeof($data);$row++){
            ?>
            <tr>
                <?php
                for($col=0;$col<sizeof($data[0]);$col++){
                ?>
                  <td><?= $data[$row][$col]; ?></td>

                <?php } ?>
            </tr>
            <?php
                }
            }
            ?>
            </tbody>
        </table>
    </div>
  </div>