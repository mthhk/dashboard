<?php

$startdate=strtotime($_POST['startdate']);
$enddate=strtotime($_POST['enddate']);
$userid=$_POST['userid'];
$newstart=date('Ymd000000',$startdate);
$newend=date('Ymd235959',$enddate);


if($startdate==true){

	if($newstart>date('Ymd000000')){
		?>
	<div class="row">
		<div class="col s12 m12 l2"></div>
		<div class="col s12 m12 l8">
				<h2 class='center mss animated infinite swing'>Error! You are selecting future date</h2>
		</div>
		<div class="col s12 m12 l2"></div>
	</div>
		<?php
	}
	else{
	$report_data=Clocking_Report($userid,$newstart,$newend,'report_link');
	$sitename=Clocking_Report($userid,$newstart,$newend,'site_name');
	$starttime=Clocking_Report($userid,$newstart,$newend,'start_clock_time');
	$endtime=Clocking_Report($userid,$newstart,$newend,'end_clock_time');
	$missedpoint=Clocking_Report($userid,$newstart,$newend,'missed_checkpoint');
	$doneby=Clocking_Report($userid,$newstart,$newend,'done_by');
	$remarks=Clocking_Report($userid,$newstart,$newend,'remarks');
	?>
	<div class="row">
		<div class="col s12 m12 l12 center">
			<h3 class='center mss'>Found <?= sizeof($report_data) ?> Reports</h3>
		</div>
	</div>

	<div class="row">

		<div class="col s12 m12 l1"> </div>
		<div class="col s12 m12 l10">
		<table>
	        <thead>
	          <tr id="clock_report_head">
	              <th>Report Link</th>
	              <th>Site Name</th>
	              <th>Start Time</th>
	              <th>End Time</th>
	              <th>Missed Check Point</th>
	              <th>Done By</th>
	              <th>Remarks</th>
	          </tr>
	        </thead>
	<?php
	for($i=0;$i<sizeof($report_data);$i++){
		switch($missedpoint[$i]){
			 case 'true':
			 $missedpoint[$i]='YES';
			 break;

			 default:
			 $missedpoint[$i]='NO';
		}
	?>

	        <tbody>
	          <tr>
	            <td><a class="waves-effect waves-light btn-large light-blue darken-3" href="<?= $report_data[$i] ?>" target="_blank"><i class="material-icons left">visibility</i>View Report</a></td>
	            <td><?= $sitename[$i] ?></td>
	            <td class="center"><?= $starttime[$i] ?></td>
	            <td class="center"><?= $endtime[$i] ?></td>
	            <td class="center"><?= $missedpoint[$i] ?></td>
	            <td class="center"><?= $doneby[$i] ?></td>
	            <td id="remarks"><?= $remarks[$i] ?></td>
	          </tr>
	        </tbody>

	<?php
		}
	?>
	</table>
	      <div class="col s12 m12 l1"> </div>

	</div>

	<?php

	}


}
?>
