<?php

////////////////////////////////////////////////////////////////  EAST  ///////////////////////////////////////////////
// DAY SHIFT DATA TO SHOW IN NIGHT TIME
if($current_time>=date('Y-m-d 17:00:00') && $current_time<=date('Y-m-d 23:59:59')){
  $ttime1=date('Y-m-d 04:00:00');
  $ttime2=date('Y-m-d 16:59:59');
}
// NIGHT SHIFT DATA TO SHOW IN DAY TIME
else{
  $ttime1=date('Y-m-d 17:00:00', strtotime('-1 day'));
  $ttime2=date('Y-m-d 03:59:59');
}
  $atime=Common_Data('cin','East',$ttime1,$ttime2,'AND attendance_id NOT IN (SELECT attendance_id FROM cout)','atime');
  $ic=Common_Data('cin','East',$ttime1,$ttime2,'AND attendance_id NOT IN (SELECT attendance_id FROM cout)','nric');
  $name=Common_Data('cin','East',$ttime1,$ttime2,'AND attendance_id NOT IN (SELECT attendance_id FROM cout)','fullname');
  $photo=Common_Data('cin','East',$ttime1,$ttime2,'AND attendance_id NOT IN (SELECT attendance_id FROM cout)','photo');
  $sitename=Common_Data('cin','East',$ttime1,$ttime2,'AND attendance_id NOT IN (SELECT attendance_id FROM cout)','sitename');
?>
<div id="oteast" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4>Officer(s) Failed to Clock Out [East]</h4>

    <div class="col s12 m12 l12">
      <table id="data" class="bordered highlight">
        <tr>
            <th class="center">Photo</th>
            <th class="center">NRIC</th>
            <th class="center">Name</th>
            <th class="center">Site</th>
            <th class="center">Checked In</th>
            <th class="center">Total Working Hours</th>
        </tr>

        <?php
        for($i=0;$i<sizeof($atime);$i++){

        $t1=strtotime($atime[$i]); //check in time 
        $t2=strtotime($current_time); //Now 
        $total_east=round(($t2-$t1)/3600,2);
          if($total_east>11.30){
            $east=true;
        ?>
        <tr>
            <td class="center"><img src="http://ireppro.com/PhotoAttendance/<?= $photo[$i] ?>" width="150" height="170" ></td>
            <td class="center"><?= $ic[$i] ?></td>
            <td class="center"><?= $name[$i] ?></td>
            <td class="center"><?= $sitename[$i]?></td>
            <td class="center"><?= $atime[$i]?></td>
            <td class="center"><?= $total_east ?> Hours</td>
        </tr>
        <?php
          }

        }
        ?>
      </table>
      </div>
  </div>

  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn red">Close</a>
  </div>

</div>

<?php
////////////////////////////////////////////////////////////////  West  /////////////////////////////////////////////////
// DAY SHIFT DATA
if($current_time>=date('Y-m-d 17:00:00') && $current_time<=date('Y-m-d 23:59:59')){
  $ttime1=date('Y-m-d 04:00:00');
  $ttime2=date('Y-m-d 16:59:59');
}
// NIGHT SHIFT DATA
else{
  $ttime1=date('Y-m-d 17:00:00', strtotime('-1 day'));
  $ttime2=date('Y-m-d 03:59:59');
}
  $atime=Common_Data('cin','West',$ttime1,$ttime2,'AND attendance_id NOT IN (SELECT attendance_id FROM cout)','atime');
  $ic=Common_Data('cin','West',$ttime1,$ttime2,'AND attendance_id NOT IN (SELECT attendance_id FROM cout)','nric');
  $name=Common_Data('cin','West',$ttime1,$ttime2,'AND attendance_id NOT IN (SELECT attendance_id FROM cout)','fullname');
  $photo=Common_Data('cin','West',$ttime1,$ttime2,'AND attendance_id NOT IN (SELECT attendance_id FROM cout)','photo');
  $sitename=Common_Data('cin','West',$ttime1,$ttime2,'AND attendance_id NOT IN (SELECT attendance_id FROM cout)','sitename');

?>
<div id="otwest" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4>Officer(s) Failed to Clock Out [West]</h4>

    <div class="col s12 m12 l12">
      <table id="data" class="bordered highlight">
        <tr>
            <th class="center">Photo</th>
            <th class="center">NRIC</th>
            <th class="center">Name</th>
            <th class="center">Site</th>
            <th class="center">Checked In</th>
            <th class="center">Total Working Hours</th>
        </tr>

        <?php
        for($i=0;$i<sizeof($atime);$i++){

        $t1=strtotime($atime[$i]);
        $t2=strtotime($current_time);
        $total_west=round(($t2-$t1)/3600,2);
          if($total_west>11.30){
            $west=true;
        ?>
        <tr>
            <td class="center"><img src="http://ireppro.com/PhotoAttendance/<?= $photo[$i] ?>" width="150" height="170" ></td>
            <td class="center"><?= $ic[$i] ?></td>
            <td class="center"><?= $name[$i] ?></td>
            <td class="center"><?= $sitename[$i]?></td>
            <td class="center"><?= $atime[$i]?></td>
            <td class="center"><?= $total_west ?> Hours</td>
        </tr>
        <?php
          }
        }
        ?>
      </table>
      </div>
  </div>

  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn red">Close</a>
  </div>

</div>



<?php
////////////////////////////////////////////////////////////////  North  /////////////////////////////////////////////////
// DAY SHIFT DATA
if($current_time>=date('Y-m-d 17:00:00') && $current_time<=date('Y-m-d 23:59:59')){
  $ttime1=date('Y-m-d 04:00:00');
  $ttime2=date('Y-m-d 16:59:59');
}
// NIGHT SHIFT DATA
else{
  $ttime1=date('Y-m-d 17:00:00', strtotime('-1 day'));
  $ttime2=date('Y-m-d 03:59:59');
}
  $atime=Common_Data('cin','North',$ttime1,$ttime2,'AND attendance_id NOT IN (SELECT attendance_id FROM cout)','atime');
  $ic=Common_Data('cin','North',$ttime1,$ttime2,'AND attendance_id NOT IN (SELECT attendance_id FROM cout)','nric');
  $name=Common_Data('cin','North',$ttime1,$ttime2,'AND attendance_id NOT IN (SELECT attendance_id FROM cout)','fullname');
  $photo=Common_Data('cin','North',$ttime1,$ttime2,'AND attendance_id NOT IN (SELECT attendance_id FROM cout)','photo');
  $sitename=Common_Data('cin','North',$ttime1,$ttime2,'AND attendance_id NOT IN (SELECT attendance_id FROM cout)','sitename');

?>
<div id="otnorth" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4>Officer(s) Failed to Clock Out [North]</h4>

    <div class="col s12 m12 l12">
      <table id="data" class="bordered highlight">
        <tr>
            <th class="center">Photo</th>
            <th class="center">NRIC</th>
            <th class="center">Name</th>
            <th class="center">Site</th>
            <th class="center">Checked In</th>
            <th class="center">Total Working Hours</th>
        </tr>

        <?php
        for($i=0;$i<sizeof($atime);$i++){

        $t1=strtotime($atime[$i]);
        $t2=strtotime($current_time);
        $total_north=round(($t2-$t1)/3600,2);
          if($total_north>11.30){
            $north=true;
        ?>
        <tr>
            <td class="center"><img src="http://ireppro.com/PhotoAttendance/<?= $photo[$i] ?>" width="150" height="170" ></td>
            <td class="center"><?= $ic[$i] ?></td>
            <td class="center"><?= $name[$i] ?></td>
            <td class="center"><?= $sitename[$i]?></td>
            <td class="center"><?= $atime[$i]?></td>
            <td class="center"><?= $total_north ?> Hours</td>
        </tr>
        <?php
          }
        }
        ?>
      </table>
      </div>
  </div>

  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn red">Close</a>
  </div>

</div>


<?php
////////////////////////////////////////////////////////////////  Central  /////////////////////////////////////////////////
// DAY SHIFT DATA
if($current_time>=date('Y-m-d 17:00:00') && $current_time<=date('Y-m-d 23:59:59')){
  $ttime1=date('Y-m-d 04:00:00');
  $ttime2=date('Y-m-d 16:59:59');
}
// NIGHT SHIFT DATA
else{
  $ttime1=date('Y-m-d 17:00:00', strtotime('-1 day'));
  $ttime2=date('Y-m-d 03:59:59');
}
  $atime=Common_Data('cin','Central',$ttime1,$ttime2,'AND attendance_id NOT IN (SELECT attendance_id FROM cout)','atime');
  $ic=Common_Data('cin','Central',$ttime1,$ttime2,'AND attendance_id NOT IN (SELECT attendance_id FROM cout)','nric');
  $name=Common_Data('cin','Central',$ttime1,$ttime2,'AND attendance_id NOT IN (SELECT attendance_id FROM cout)','fullname');
  $photo=Common_Data('cin','Central',$ttime1,$ttime2,'AND attendance_id NOT IN (SELECT attendance_id FROM cout)','photo');
  $sitename=Common_Data('cin','Central',$ttime1,$ttime2,'AND attendance_id NOT IN (SELECT attendance_id FROM cout)','sitename');

?>
<div id="otcentral" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4>Officer(s) Failed to Clock Out [Central]</h4>

    <div class="col s12 m12 l12">
      <table id="data" class="bordered highlight">
        <tr>
            <th class="center">Photo</th>
            <th class="center">NRIC</th>
            <th class="center">Name</th>
            <th class="center">Site</th>
            <th class="center">Checked In</th>
            <th class="center">Total Working Hours</th>
        </tr>

        <?php
        for($i=0;$i<sizeof($atime);$i++){

        $t1=strtotime($atime[$i]);
        $t2=strtotime($current_time);
        $total_central=round(($t2-$t1)/3600,2);
          if($total_central>11.30){
            $central=true;
        ?>
        <tr>
            <td class="center"><img src="http://ireppro.com/PhotoAttendance/<?= $photo[$i] ?>" width="150" height="170" ></td>
            <td class="center"><?= $ic[$i] ?></td>
            <td class="center"><?= $name[$i] ?></td>
            <td class="center"><?= $sitename[$i]?></td>
            <td class="center"><?= $atime[$i]?></td>
            <td class="center"><?= $total_central ?> Hours</td>
        </tr>
        <?php
          }
        }
        ?>
      </table>
      </div>
  </div>

  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn red">Close</a>
  </div>

</div>
