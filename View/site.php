<div class="col s12 m12 l4"> </div>
<div class="col s12 m12 l4">

  <div class="card" id="pwcard">

    <div class="card-content">
      <span class="card-title center">Add New Site</span>

      <form action="Controller/update_controller.php" method="post">

        <input type="hidden" name="email" value="<?= $_SESSION['email'] ?>">

        <div class="input-field col s12 m12 l12">
          <input id="current_pass" name="current_pass" type="text" class="validate" required>
          <label for="current_pass">Site ID (Eg. 1592)</label>
        </div>

        <div class="input-field col s8 m8 l8">
          <input id="new_pass" name="new_pass" type="text" class="validate" required>
          <label for="new_pass">Site Name</label>
        </div>
        <div class="input-field col s4 m4 l4">
          <input id="new_pass" name="new_pass" type="text" class="validate" required>
          <label for="new_pass">Site Acronym</label>
        </div>

        <div class="input-field col s12 m12 l12">
          <select>
            <option value="" disabled selected>Site Cluster</option>
            <option value="1">East</option>
            <option value="1">West</option>
            <option value="1">North</option>
            <option value="1">Central</option>
          </select>
        </div>

        <div class="input-field col s12 m6 l6">
          <input id="new_pass" name="new_pass" type="text" class="validate" required>
          <label for="new_pass">Day Officers (Eg. 4)</label>
        </div>
        <div class="input-field col s12 m6 l6">
          <input id="new_pass" name="new_pass" type="text" class="validate" required>
          <label for="new_pass">Night Officers (Eg. 2)</label>
        </div>


        <!-- <div class="input-field col s4 m12 l12">
          <input id="new_pass" name="new_pass" type="text" class="validate" required>
          <label for="new_pass">Required Night Officers (Eg. 2)</label>
        </div> -->

        <div class="input-field col s12 m12 l12 center">
          <button class="btn-large waves-effect waves-light light-blue darken-1" type="submit" name="action">Add Site
            <i class="material-icons right">add_box</i>
          </button>
          <button class="btn btn-floating waves-effect waves-light light-blue darken-1" type="reset" name="action"><i class="material-icons right">refresh</i>
          </button>
        </div>
      </form>
    </div>

  </div>

</div>

<div class="col s12 m12 l4"> </div>
