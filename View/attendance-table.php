<?php

$startdate=strtotime($_POST['startdate']);
$enddate=strtotime($_POST['enddate']);

$newstart=date('Ymd000000',$startdate);
$newend=date('Ymd235959',$enddate);

if($startdate==true){

	if($newstart>date('Ymd000000')){
		?>
	<div class="row">
		<div class="col s12 m12 l2"></div>
		<div class="col s12 m12 l8">
				<h2 class='center mss animated infinite swing'>Error! You are selecting future date</h2>
		</div>
		<div class="col s12 m12 l2"></div>
	</div>
		<?php
	}
	else{

	$report_data=Report_Data($newstart,$newend);
	?>
	<div class="row">
		<div class="col s12 m12 l12 center">
			<h3 class='center mss'>Found <?= sizeof($report_data->Data->Attendance) ?> Records</h3>
		</div>
	</div>
	<div class="row">

		<div class="col s12 m12 l2"> </div>
		<div class="col s12 m12 l8">
		<table>
	        <thead>
	          <tr id="attendance_report_head">
				  <th>Photo</th>
	              <th>Fullname</th>
	              <th>NRIC</th>
	              <th>Attendance Time</th>
	              <th>Action</th>
	              <th>Site</th>
	          </tr>
	        </thead>
	<?php
	for($i=0;$i<sizeof($report_data->Data->Attendance);$i++){
	?>

	        <tbody>
	          <tr>
							<!-- <td class="center"><img src="http://ireppro.com/PhotoAttendance/<?= $report_data->Data->Attendance[$i]->Photo ?>" width="150" height="170" ></td> -->
							<td><?= $report_data->Data->Attendance[$i]->Photo ?></td>
	            <td><?= $report_data->Data->Attendance[$i]->FullName ?></td>
	            <td class="center"><?= $report_data->Data->Attendance[$i]->NRIC ?></td>
	            <td class="center"><?= $report_data->Data->Attendance[$i]->ATime ?></td>
	            <td class="center"><?= $report_data->Data->Attendance[$i]->Action ?></td>
	            <td class="center"><?= $report_data->Data->Attendance[$i]->Site ?></td>
	          </tr>
	        </tbody>

	<?php
		}
	?>
	</table>
	      <div class="col s12 m12 l2"> </div>

	</div>

	<?php

	}


}
?>
