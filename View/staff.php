<?php

  $cluster=Array_Data('site','site_id','office','site_cluster');
  $day_officer=Array_Data('site','site_cluster','Ops Office','day_officer');
  $night_officer=Array_Data('site','site_cluster','Ops Office','night_officer');
  $num_west_site=Array_Data('site','site_cluster','Ops Office','site_id');
  $west_site_list=Array_Data('site','site_cluster','Ops Office','site_name');
  $west_site_syn=Array_Data('site','site_cluster','Ops Office','site_syn');
  $timeslot=Array_Data('site','site_cluster','Ops Office','timeslot_id');
  $Phone=Array_Data('site','site_cluster','Ops Office','Phone');

  for($i=0;$i<sizeof($num_west_site);$i++){

      $siteid=$num_west_site[$i];

      $time1=date('Y-m-d 06:00:00');//Day
      $time2=date('Y-m-d 16:59:59');//Day

      if($current_time>=date('Y-m-d 17:00:00') && $current_time<=date('Y-m-d 23:59:59')){
        $time3=date('Y-m-d 17:00:00'); //night
        $time4=date('Y-m-d 03:59:59', strtotime('+1 day')); //night
      }
      else{
        $time3=date('Y-m-d 17:00:00', strtotime('-1 day')); //night
        $time4=date('Y-m-d 03:59:59'); //night
      }

      $am_attendance=Check_In_Out('cin',$siteid,'atime',$time1,$time2,'attendance_id');//table, condition 1, condition 2, Time 1, Time 2, Pull datalist
      $pm_attendance=Check_In_Out('cin',$siteid,'atime',$time3,$time4,'attendance_id');

      $time=Check_In_Out('cin',$siteid,'atime',$time1,$time2,'atime');
      $nric=Check_In_Out('cin',$siteid,'atime',$time1,$time2,'nric');
      $photo=Check_In_Out('cin',$siteid,'atime',$time1,$time2,'photo');
      $name=Check_In_Out('cin',$siteid,'atime',$time1,$time2,'fullname');

      $time_night=Check_In_Out('cin',$siteid,'atime',$time3,$time4,'atime');
      $nric_night=Check_In_Out('cin',$siteid,'atime',$time3,$time4,'nric');
      $photo_night=Check_In_Out('cin',$siteid,'atime',$time3,$time4,'photo');
      $name_night=Check_In_Out('cin',$siteid,'atime',$time3,$time4,'fullname');

      Update_Officer($siteid,'today_officer',sizeof($am_attendance));
      Update_Officer($siteid,'tonight_officer',sizeof($pm_attendance));

    if($current_time>=$timing1 && $current_time<=$timing2){
        $color='oebar';
    }

    else{
      $color='oebar';
    }

?>

<div class="col s12 m6 l4">
    <div id="<?= $color ?>" class="<?= $animation ?>">
      <table>
          <tr>

              <td class="center">
                <a href="#modalwestd<?= $i ?>" class="modal-trigger">
                <p id="date" class="center">[Day]</p>
                <span class="num">
                <?php
                    echo sizeof($am_attendance)."/".$day_officer[$i];
                ?>
                </span>
                </a>
              </td>

              <td class="center">
                <a href="#modalwestn<?= $i ?>" class="modal-trigger">
                <p id="date" class="center">[Night]</p>
                <span class="num">
                <?php
                    echo sizeof($pm_attendance)."/".$night_officer[$i];
                ?>
                </span>
                </a>
              </td>
          </tr>
          <tr>
              <td colspan="3" class="center" id="sitename"><?= $west_site_list[$i]." [".$west_site_syn[$i]."]" ?></td>
          </tr>
      </table>
    </div><br>
</div>


<div id="modalwestd<?= $i ?>" class="modal bottom-sheet">
  <div class="modal-content">
    <h5><?= $west_site_list[$i] ?> [Attendance Status]</h5>

    <div class="col s12 m12 l12">
      <table id="data" class="bordered highlight">
        <tr>
            <th class="center">Photo</th>
            <th class="center">NRIC</th>
            <th class="center">Name</th>
            <th class="center">Attendance Time</th>
        </tr>
<?php

if(sizeof($am_attendance)==0){
?>
        <tr>
            <td class="center" colspan="4"><h4 class="red-text">Sorry! No Data To Show</h4></td>
        </tr>
<?php
}
for($ii=0;$ii<sizeof($am_attendance);$ii++){ ?>
        <tr>
            <td class="center"><img src="http://ireppro.com/PhotoAttendance/<?= $photo[$ii] ?>" width="150" height="170" ></td>
            <td class="center"><?= $nric[$ii] ?></td>
            <td class="center"><?= $name[$ii] ?></td>
            <td class="center"><?= $time[$ii] ?></td>
        </tr>
<?php
}
?>
    </table>
  </div>

</div>
<div class="modal-footer">
  <a href="#!" class="modal-action modal-close waves-effect waves-green btn red">Close</a>
</div>
</div>


<div id="modalwestn<?= $i ?>" class="modal bottom-sheet">
  <div class="modal-content">
    <h5><?= $west_site_list[$i] ?> [Attendance Status]</h5>

    <div class="col s12 m12 l12">
      <table id="data" class="bordered highlight">
        <tr>
            <th class="center">Photo</th>
            <th class="center">NRIC</th>
            <th class="center">Name</th>
            <th class="center">Attendance Time</th>
        </tr>
<?php
if(sizeof($pm_attendance)==0){
?>
  <tr>
      <td class="center" colspan="4"><h4 class="red-text  ">Sorry! No Data To Show</h4></td>
  </tr>
<?php
}
for($ii=0;$ii<sizeof($pm_attendance);$ii++){ ?>
<tr>
    <td class="center"><img src="http://ireppro.com/PhotoAttendance/<?= $photo_night[$ii] ?>" width="150" height="170" ></td>
    <td class="center"><?= $nric_night[$ii] ?></td>
    <td class="center"><?= $name_night[$ii] ?></td>
    <td class="center"><?= $time_night[$ii] ?></td>
</tr>
<?php
}
?>
    </table>
  </div>

</div>
<div class="modal-footer">
  <a href="#!" class="modal-action modal-close waves-effect waves-green btn red">Close</a>
</div>
</div>

<?php } ?>
