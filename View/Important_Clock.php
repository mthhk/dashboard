<?php
/////////////////////////////// DATA Retrieving /////////////////////////////////
$siteid=clock_site('site','site_priority','1','site_id');
$sitename=clock_site('site','site_priority','1','site_name');
$num_clock=clock_site('site','site_priority','1','num_clocking');
$today_clock=clock_site('site','site_priority','1','today_clock');
$timing=clock_site('site','site_priority','1','timing');

for($i=0;$i<sizeof($siteid);$i++){

  $sid=$siteid[$i];
  //echo $sid;

  $param_time1=date('Y-m-d 00:00:00');
  $param_time2=date('Y-m-d 23:59:59');

  $param_time3=date('Y-m-d 00:00:00',strtotime('-1 day'));
  $param_time4=date('Y-m-d 23:59:59',strtotime('-1 day'));

  // today clocking
  $data=clock_data('c.cus_id='.$sid,$param_time1,$param_time2,'report_id');
  $report_link=clock_data('c.cus_id='.$sid,$param_time1,$param_time2,'report_link');
  $route=clock_data('c.cus_id='.$sid,$param_time1,$param_time2,'contract_title');
  $done_by=clock_data('c.cus_id='.$sid,$param_time1,$param_time2,'done_by');
  $start_at=clock_data('c.cus_id='.$sid,$param_time1,$param_time2,'start_clock_time');
  $end_at=clock_data('c.cus_id='.$sid,$param_time1,$param_time2,'end_clock_time');
  $remarks=clock_data('c.cus_id='.$sid,$param_time1,$param_time2,'remarks');
  $missed_points=clock_data('c.cus_id='.$sid,$param_time1,$param_time2,'missed_checkpoint');
  $completed_data=sizeof($data);

  // Ytd Clocking
  $ytdData=clock_data('c.cus_id='.$sid,$param_time3,$param_time4,'report_id');
  $ytdLink=clock_data('c.cus_id='.$sid,$param_time3,$param_time4,'report_link');
  $ytdRoute=clock_data('c.cus_id='.$sid,$param_time3,$param_time4,'contract_title');
  $ytdDoneby=clock_data('c.cus_id='.$sid,$param_time3,$param_time4,'done_by');
  $ytdStart=clock_data('c.cus_id='.$sid,$param_time3,$param_time4,'start_clock_time');
  $ytdEnd=clock_data('c.cus_id='.$sid,$param_time3,$param_time4,'end_clock_time');
  $ytdRemark=clock_data('c.cus_id='.$sid,$param_time3,$param_time4,'remarks');
  $ytdMissed=clock_data('c.cus_id='.$sid,$param_time3,$param_time4,'missed_checkpoint');
  $ytdCompletedata=sizeof($ytdData);


  

  $time_array=unserialize($timing[$i]);

  include 'condition.php';

?>
<div class="col s12 m12 l3" style="width: 450px; margin-bottom: 10px; margin-right: 20px;" id="<?= $color ?>">
  <table>
    <tr>
        <td colspan="4" class="center" id="clock_sitename"><a href="#modaleast<?= $i ?>" class="modal-trigger tooltipped" data-position="left" data-html="true" data-tooltip="<?php echo "<h5>Clocking Hrs</h5>"; for($array=0;$array<sizeof($time_array);$array++){echo "<h6>".$time_array[$array]."</h6><br/>"; } ?>"><?= $sitename[$i] ?></a></td>
    </tr>
    <!-- fixed column -->
    <tr>
      <td class="center" id="timing"><a class="tooltipped" data-position="top" data-tooltip="Complete">[ C ]</a></td>
      <td class="center" id="timing"><a class="tooltipped" data-position="top" data-tooltip="Incomplete Route">[ M ]</a></td>
      <td class="center" id="timing"><a class="tooltipped" data-position="top" data-tooltip="Total">[ T ]</a></td>
    </tr>
    <!-- count number of time doing clocking -->
    <tr>
      <td class="center" id="timing"><a class="btn-floating waves-effect blue-grey darken-3"><?= $completed_data ?></a></td>      
      <td class="center" id="timing"><a class="btn-floating waves-effect blue-grey darken-3 <?= $animation ?>"><?= sizeof($total_missed) ?></a></td>
      <td class="center" id="timing"><a class="btn-floating waves-effect blue-grey darken-3"><?= $num_clock[$i] ?></a></td>
    </tr>
  </table>
</div>


  <div id="modaleast<?= $i ?>" class="modal modal-fixed-footer">
      <div class="modal-content">
        <h4><?= $sitename[$i] ?> [Clocking Status]</h4>

        <div class="row">
            <div class="col s12 m12 l12">
              <div class="row">
                  <div class="col s12 m12 l12"><h5 id="tdyclocking" class="center">Today Clocking</h5></div>
              </div>
              <table id="data" class="bordered highlight">
                <tr>
                    <th class="center">Report Link</th>
                    <th class="center">Route</th>
                    <th class="center">Done By</th>
                    <th class="center">Start At</th>
                    <th class="center">End At</th>
                    <th class="center">Remarks</th>
                </tr>
                <?php
                if(empty($data)){
                ?>
                <tr>
                    <td class="center" colspan="6"><h4 class="red-text animated infinite fadeIn">Sorry! No Record Found</h4></td>
                </tr>
                <?php
                }
                for($j=0;$j<$completed_data;$j++){
                ?>
                <tr>
                    <td class="center"><a href="<?= $report_link[$j] ?>" target="__Blank"> View Report </a></td>
                    <td class="center"><?= $route[$j] ?></td>
                    <td class="center"><?= $done_by[$j] ?></td>
                    <td class="center"><?= $start_at[$j] ?></td>
                    <td class="center"><?= $end_at[$j] ?></td>
                    <td class="center"><?= $remarks[$j] ?></td>
                </tr>
            <?php } ?>
            </table>
        </div>
      </div>

      <div class="row">
            <div class="col s12 m12 l12">
              <div class="row">
                  <div class="col s12 m12 l12"><h5 id="tdyclocking" class="center"><?= date('d-M-Y',strtotime('-1 day')) ?></h5></div>
              </div>
              <table id="data" class="bordered highlight">
                <tr>
                    <th class="center">Report Link</th>
                    <th class="center">Route</th>
                    <th class="center">Done By</th>
                    <th class="center">Start At</th>
                    <th class="center">End At</th>
                    <th class="center">Remarks</th>
                </tr>
                <?php
                if(empty($ytdData)){
                ?>
                <tr>
                    <td class="center" colspan="6"><h4 class="red-text animated infinite fadeIn">Sorry! No Record Found</h4></td>
                </tr>
                <?php
                }
                for($j=0;$j<$ytdCompletedata;$j++){
                ?>
                <tr>
                    <td class="center"><a href="<?= $report_link[$j] ?>" target="__Blank"> View Report </a></td>
                    <td class="center"><?= $ytdRoute[$j] ?></td>
                    <td class="center"><?= $ytdDoneby[$j] ?></td>
                    <td class="center"><?= $ytdStart[$j] ?></td>
                    <td class="center"><?= $ytdEnd[$j] ?></td>
                    <td class="center"><?= $ytdRemark[$j] ?></td>

                </tr>
                <?php } ?>
                </table>
          </div>
      </div>

    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-action modal-close waves-effect waves-red btn red darken-4">Close</a>
    </div>
    </div>
<?php
}
?>
