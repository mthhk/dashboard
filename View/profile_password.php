<div class="col s12 m6 l4 offset-l4">

  <div class="card" id="pwcard">

    <div class="card-content">
      <span class="card-title center">Change Password</span>

      <form action="Controller/update_controller.php" method="post">

        <input type="hidden" name="email" value="<?= $_SESSION['email'] ?>">

        <div class="input-field col s12 m12 l12">
          <input id="current_pass" name="current_pass" type="password" class="validate" required>
          <label for="current_pass">Current Password</label>
        </div>

        <div class="input-field col s12 m12 l12">
          <input id="new_pass" name="new_pass" type="password" class="validate" required>
          <label for="new_pass">New Password</label>
        </div>

        <div class="input-field col s12 m12 l12">
          <input id="confirm_pass" name="cfm_pass" type="password" class="validate" required>
          <label for="confirm_pass">Confirm Password</label>
        </div>

        <div class="input-field col s12 m12 l12 center">
          <button class="btn-large waves-effect waves-light light-blue darken-1" type="submit" name="action">Update Password
            <i class="material-icons right">lock</i>
          </button>
        </div>
      </form>
    </div>

  </div>

</div>
