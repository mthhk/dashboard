<!-- //////////////////////////////////////////////////////////////////////////  EAST  //////////////////////////////////////////////////// -->
<?php
if($current_time>=$time1 && $current_time<=$time2){
$ic=Common_Data('cin','East',$time1,$time2,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','nric');
$photo=Common_Data('cin','East',$time1,$time2,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','photo');
$sitename=Common_Data('cin','East',$time1,$time2,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','sitename');
$name=Common_Data('cin','East',$time1,$time2,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','fullname');
//echo $time1." - ".$time2;
}
else{
  $time4=date('Y-m-d 03:59:59', strtotime('+1 day'));
  $ic=Common_Data('cin','East',$time3,$time4,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','nric');
  $photo=Common_Data('cin','East',$time3,$time4,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','photo');
  $sitename=Common_Data('cin','East',$time3,$time4,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','sitename');
  $name=Common_Data('cin','East',$time3,$time4,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','fullname');
  //print_r($ic);
}
$pattern = "/[GSFgsf]+[0-9]{7}[A-Za-z]$/";
$east_wrong=IC_Validator($ic);
//print_r($east_wrong);
?>
<div id="eastwrongic" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4>Officer(s) Wrong NRIC [East]</h4>

    <div class="col s12 m12 l12">
      <table id="data" class="bordered highlight">
        <tr>
           <!--  <th class="center">Photo</th> -->
            <th class="center">Photo</th>
            <th class="center">NRIC</th>
            <th class="center">Name</th>
            <th class="center">Site</th>
        </tr>
        <?php
        for($i=0;$i<sizeof($ic);$i++){
          if(!preg_match($pattern,$ic[$i])){
        ?>
        <tr>
            <td class="center"><img src="http://ireppro.com/PhotoAttendance/<?= $photo[$i] ?>" width="150" height="170" ></td>
            <td class="center"><?= $ic[$i] ?></td>
            <td class="center"><?= $name[$i] ?></td>
            <td class="center"><?= $sitename[$i]?></td>
        </tr>
        <?php
          }
        }
        ?>
      </table>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn red">Close</a>
  </div>
</div>

<!-- //////////////////////////////////////////////////////////////////////  WEST  ////////////////////////////////////////////////////// -->
<?php
if($current_time>=$time1 && $current_time<=$time2){
$westic=Common_Data('cin','West',$time1,$time2,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','nric');
$westphoto=Common_Data('cin','West',$time1,$time2,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','photo');
$westsitename=Common_Data('cin','West',$time1,$time2,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','sitename');
$westname=Common_Data('cin','West',$time1,$time2,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','fullname');
}
else {
  $westic=Common_Data('cin','West',$time3,$time4,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','nric');
  $westphoto=Common_Data('cin','West',$time3,$time4,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','photo');
  $westsitename=Common_Data('cin','West',$time3,$time4,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','sitename');
  $westname=Common_Data('cin','West',$time3,$time4,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','fullname');
}
$west_wrong=IC_Validator($westic);
?>
<div id="westwrongic" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4>Officer(s) Wrong NRIC [West]</h4>

    <div class="col s12 m12 l12">
      <table id="data" class="bordered highlight">
        <tr>
            <th class="center">Photo</th>
            <th class="center">NRIC</th>
            <th class="center">Name</th>
            <th class="center">Site</th>
        </tr>
        <?php
        for($i=0;$i<sizeof($westic);$i++){
          if(!preg_match($pattern,$westic[$i])){
        ?>
        <tr>
            <td class="center"><img src="http://ireppro.com/PhotoAttendance/<?= $westphoto[$i] ?>" width="150" height="170" ></td>
            <td class="center"><?= $westic[$i] ?></td>
            <td class="center"><?= $westname[$i] ?></td>
            <td class="center"><?= $westsitename[$i]?></td>
        </tr>
        <?php
          }
        }
        ?>
      </table>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn red">Close</a>
  </div>
</div>

<!-- //////////////////////////////////////////////////////////////////////  NORTH  ////////////////////////////////////////////////////// -->
<?php
if($current_time>=$time1 && $current_time<=$time2){
$northic=Common_Data('cin','North',$time1,$time2,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','nric');
$northphoto=Common_Data('cin','North',$time1,$time2,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','photo');
$northsitename=Common_Data('cin','North',$time1,$time2,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','sitename');
$northname=Common_Data('cin','North',$time1,$time2,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','fullname');
}
else{
  $northic=Common_Data('cin','North',$time3,$time4,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','nric');
  $northphoto=Common_Data('cin','North',$time3,$time4,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','photo');
  $northsitename=Common_Data('cin','North',$time3,$time4,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','sitename');
  $northname=Common_Data('cin','North',$time3,$time4,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','fullname');
}
$north_wrong=IC_Validator($northic);
?>
<div id="northwrongic" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4>Officer(s) Wrong NRIC [North]</h4>

    <div class="col s12 m12 l12">
      <table id="data" class="bordered highlight">
        <tr>
            <th class="center">Photo</th>
            <th class="center">NRIC</th>
            <th class="center">Name</th>
            <th class="center">Site</th>
        </tr>
        <?php
        for($i=0;$i<sizeof($northic);$i++){
          if(!preg_match($pattern,$northic[$i])){
        ?>
        <tr>
            <td class="center"><img src="http://ireppro.com/PhotoAttendance/<?= $northphoto[$i] ?>" width="150" height="170" ></td>
            <td class="center"><?= $northic[$i] ?></td>
            <td class="center"><?= $northname[$i] ?></td>
            <td class="center"><?= $northsitename[$i]?></td>
        </tr>
        <?php
          }
        }
        ?>
      </table>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn red">Close</a>
  </div>
</div>


<!-- //////////////////////////////////////////////////////////////////////  Central  ////////////////////////////////////////////////////// -->
<?php
if($current_time>=$time1 && $current_time<=$time2){
$centralic=Common_Data('cin','Central',$time1,$time2,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','nric');
$centralphoto=Common_Data('cin','Central',$time1,$time2,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','photo');
$centralsitename=Common_Data('cin','Central',$time1,$time2,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','sitename');
$centralname=Common_Data('cin','Central',$time1,$time2,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','fullname');
//echo sizeof($centralic);
}
else {
  $centralic=Common_Data('cin','Central',$time3,$time4,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','nric');
  $centralphoto=Common_Data('cin','Central',$time3,$time4,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','photo');
  $centralsitename=Common_Data('cin','Central',$time3,$time4,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','sitename');
  $centralname=Common_Data('cin','Central',$time1,$time2,'AND attendance_id NOT IN(SELECT attendance_id FROM cout)','fullname');
}
$central_wrong=IC_Validator($centralic);
?>
<div id="centralwrongic" class="modal modal-fixed-footer">
  <div class="modal-content">
    <h4>Officer(s) Wrong NRIC [Central]</h4>

    <div class="col s12 m12 l12">
      <table id="data" class="bordered highlight">
        <tr>
            <th class="center">Photo</th>
            <th class="center">NRIC</th>
            <th class="center">Name</th>
            <th class="center">Site</th>
        </tr>
        <?php
        for($i=0;$i<sizeof($centralic);$i++){
          if(!preg_match($pattern,$centralic[$i])){
        ?>
        <tr>
            <td class="center"><img src="http://ireppro.com/PhotoAttendance/<?= $centralphoto[$i] ?>" width="150" height="170" ></td>
            <td class="center"><?= $centralic[$i] ?></td>
            <td class="center"><?= $centralname[$i] ?></td>
            <td class="center"><?= $centralsitename[$i]?></td>
        </tr>
        <?php
          }
        }
        ?>
      </table>
    </div>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn red">Close</a>
  </div>
</div>
