<div class="row">
 		<div class="col s12 m12 l3"> </div>
 		<div class="col s12 m12 l6 center">
 			<h3>View Attendance Report</h3>
 		</div>
 		<div class="col s12 m12 l3"> </div>
</div>

<form action="report.php" method="POST">

<div class="row">
 		<div class="col s12 m12 l4"> </div>

 		<div class="input-field col s12 m12 l2">
          <i class="material-icons prefix">access_alarm</i>
          <input id="icon_prefix" name="startdate" type="text" class="datepicker" class="validate">
          <label for="icon_prefix">Date From</label>
    </div>

 		<div class="input-field col s12 m12 l2">
          <i class="material-icons prefix">access_alarm</i>
          <input id="icon_prefix" name="enddate" type="text" class="datepicker" class="validate">
          <label for="icon_prefix">Date To</label>
        </div>

 		<div class="col s12 m12 l4"> </div>
</div>

<div class="row">
	<div class="col s12 m12 l4"> </div>

	<div class="col s12 m12 l4 center">
		<button class="btn-large waves-effect waves-light" type="submit" name="submit" id="searchbtn">Filter
    		<i class="material-icons right">search</i>
  	</button>
	</div>

	<div class="col s12 m12 l4"> </div>

</div>
</form>
