<?php
   // include("irep/Controller/api.php");
  // include("/irep/Controller/db.php");
 
  $day_officer=Array_Data('site','site_cluster','East','day_officer');
  $night_officer=Array_Data('site','site_cluster','East','night_officer');
  $num_east_site=Array_Data('site','site_cluster','East','site_id');
  $east_site_list=Array_Data('site','site_cluster','East','site_name');
  $east_site_syn=Array_Data('site','site_cluster','East','site_syn');
  $em=Array_Data('site','site_cluster','East','mid_shift');
  $em_officer=Array_Data('site','site_cluster','East','mid_officer');
  $timeslot=Array_Data('site','site_cluster','East','timeslot_id');
  $phone=Array_Data('site','site_cluster','East','Phone');


  for($i=0;$i<sizeof($num_east_site);$i++){

      $siteid=$num_east_site[$i];
      $ems=$em[$i];
      $emo=$em_officer[$i];
      $time1=date('Y-m-d 04:00:00');//Day
      $time2=date('Y-m-d 16:59:59');//Day
      $m_time1=date('Y-m-d 09:30:00');//Day

          if($current_time>=date('Y-m-d 17:00:00') && $current_time<=date('Y-m-d 23:59:59')){
            $time3=date('Y-m-d 17:00:00'); //night
            $time4=date('Y-m-d 03:59:59', strtotime('+1 day')); //night
          }
          else{
            $time3=date('Y-m-d 17:00:00', strtotime('-1 day')); //night
            $time4=date('Y-m-d 03:59:59'); //night
          }

      $am_attendance=Check_In_Out('cin',$siteid,'atime',$time1,$time2,'attendance_id');//table, condition 1, condition 2, Time 1, Time 2, Pull datalist
      $mid_attendance=Check_In_Out('cin',$siteid,'atime',$m_time1,$time2,'attendance_id');//table, condition 1, condition 2, Time 1, Time 2, Pull datalist
      $pm_attendance=Check_In_Out('cin',$siteid,'atime',$time3,$time4,'attendance_id');

      $time=Check_In_Out('cin',$siteid,'atime',$time1,$time2,'atime');
      $nric=Check_In_Out('cin',$siteid,'atime',$time1,$time2,'nric');
      $photo=Check_In_Out('cin',$siteid,'atime',$time1,$time2,'photo');
      $name=Check_In_Out('cin',$siteid,'atime',$time1,$time2,'fullname');

      $midtime=Check_In_Out('cin',$siteid,'atime',$m_time1,$time2,'atime');
      $midnric=Check_In_Out('cin',$siteid,'atime',$m_time1,$time2,'nric');
      $midphoto=Check_In_Out('cin',$siteid,'atime',$m_time1,$time2,'photo');
      $midname=Check_In_Out('cin',$siteid,'atime',$m_time1,$time2,'fullname');

      $time_night=Check_In_Out('cin',$siteid,'atime',$time3,$time4,'atime');
      $nric_night=Check_In_Out('cin',$siteid,'atime',$time3,$time4,'nric');
      $photo_night=Check_In_Out('cin',$siteid,'atime',$time3,$time4,'photo');
      $name_night=Check_In_Out('cin',$siteid,'atime',$time3,$time4,'fullname');

      

      $T1_1=date('Y-m-d 06:00:00');
      $T2_1=date('Y-m-d 07:00:00');
      $T3_1=date('Y-m-d 07:30:00');
      $T4_1=date('Y-m-d 08:00:00');
      $T7_1=date('Y-m-d 08:45:00');
      $T8_1=date('Y-m-d 20:00:00');
      $T9_1=date('Y-m-d 07:00:00');
      $T10_1=date('Y-m-d 06:30:00');

      $T1_2=date('Y-m-d 18:00:00');
      $T2_2=date('Y-m-d 19:00:00');
      $T3_2=date('Y-m-d 19:30:00');
      $T4_2=date('Y-m-d 20:00:00');
      $T9_2=date('Y-m-d 22:00:00');
      $T10_2=date('Y-m-d 18:00:00');
      
      //echo $siteid;

      Update_Officer($siteid,'today_officer',sizeof($am_attendance));
      Update_Officer($siteid,'tonight_officer',sizeof($pm_attendance));

/////////////////////////////////// Day Shift ////////////////////////////////////////////
          if($current_time>=$timing1 && $current_time<=$timing2){


              if(sizeof($pm_attendance)>=1){

                for($ii=0;$ii<sizeof($time_night);$ii++){

                  $t1=strtotime($time_night[$ii]);
                  $t2=strtotime($current_time);
                  $total_east=round(($t2-$t1)/3600,2);

                  //////////////// checking 12 hours to blink /////////////////
                  if($total_east>12){
                    $animation='animated infinite fadeIn';
                  }

                  else{
                    $animation=' ';
                  }
                }
              }
              else{
                    $animation=' ';
               }
               ///////////////////////////////////////////////////////////////////////////////
              if(sizeof($am_attendance)<$day_officer[$i]){

                  switch($siteid){

                    case '3282':
                      if($current_time>$T4_1 && $current_time<=date('Y-m-d 09:29:59')){
                        if(sizeof($am_attendance)>=3){
                          $color='greenbox';
                          $icon='check_circle';
                        }
                        else{
                          $color='redbox';
                          $icon='check_circle'; 
                        }
                      }
                      elseif($current_time>$T4_1 && $current_time<=date('Y-m-d 09:59:59')){
                          $color='yellowbox';
                          $icon='check_circle';
                      }
                      elseif($current_time>$T4_1 && $current_time>=date('Y-m-d 10:00:00')){
                          $color='redbox';
                          $icon='check_circle';
                      };
                    break;

                    default:
                      switch ($timeslot[$i]) {
                        case 'T1':      //$T1_1=date('Y-m-d 06:00:00');
                        case 'T2':      //$T2_1=date('Y-m-d 07:00:00');
                        case 'T3':      //$T3_1=date('Y-m-d 07:30:00');
                        case 'T4':      //$T4_1=date('Y-m-d 08:00:00');
                        case 'T7':      //$T7_1=date('Y-m-d 08:45:00');
                        case 'T8':      //$T8_1=date('Y-m-d 20:00:00');
                        case 'T9':      //$T9_1=date('Y-m-d 07:00:00');
                        case 'T10':     //$T10_1=date('Y-m-d 06:30:00');
                          if($current_time>$T1_1 || $current_time>$T2_1 || $current_time>$T3_1 || $current_time>$T4_1 || $current_time>$T7_1 || $current_time>$T8_1 || $current_time>$T9_1 || $current_time>$T10_1){
                            $color='redbox';
                            $icon='cancel';
                            // $animation=' ';
                          }
                          else{
                            $color='yellowbox';
                            $icon='remove_circle';
                            // $animation=' ';
                          };
                          break;
                      };
                      break;
                  }
              }
              ////////////////////////////////////////////////////////////////////////

              elseif(sizeof($am_attendance)>$day_officer[$i]){

                switch($siteid){
                  case '3282':
                    $color='greenbox';
                    $icon='add_circle';
                  break;

                  default:
                    $color='bluebox';
                    $icon='add_circle';
                }        
                
              }
              ///////////////////////////////////////////////////////////////////////

              elseif(sizeof($am_attendance)==$day_officer[$i]){
                $color='greenbox';
                $icon='check_circle';
              }    
          }

/////////////////////////////////////////////// Night Shift ///////////////////////////////////////////////////
          else{

              if(sizeof($am_attendance)>=1){

                  for($ii=0;$ii<sizeof($time);$ii++){

                  $t1=strtotime($time[$ii]);
                  $t2=strtotime($current_time);
                  $total_east=round(($t2-$t1)/3600,2);
                  ///////////////////////// Checking more than 12 hours to blink //////////////////////////
                      if($total_east>12){
                      $animation='animated infinite fadeIn';
                      }

                      else{
                      $animation=' ';
                      }
                  }
              }

              else{
                  $animation=' ';
              }

              if(sizeof($pm_attendance)<$night_officer[$i]){
                  switch ($timeslot[$i]) {

                      case 'T1':    // $T1_2=date('Y-m-d 18:00:00');
                          if($current_time>$T1_2){
                            $color='redbox';
                            $icon='cancel';
                          }else{ $color='yellowbox'; };
                        break;
                      case 'T2':    // $T2_2=date('Y-m-d 19:00:00');
                          if($current_time>$T2_2){
                            $color='redbox';
                            $icon='cancel';
                          }else{ $color='yellowbox'; };
                        break;
                      case 'T3':    // $T3_2=date('Y-m-d 19:30:00'); 
                          if($current_time>$T3_2){
                                $color='redbox';
                                $icon='cancel';
                              }else{ $color='yellowbox'; };
                        break;
                      case 'T4':    // $T4_2=date('Y-m-d 20:00:00');
                          if($current_time>$T3_2){
                                $color='redbox';
                                $icon='cancel';
                              }else{ $color='yellowbox'; };
                        break;
                      case 'T9':    // $T9_2=date('Y-m-d 22:00:00');
                          if($current_time>$T9_2){
                                $color='redbox';
                                $icon='cancel';
                              }else{ $color='yellowbox'; };
                        break;
                      case 'T10':   // $T10_2=date('Y-m-d 18:00:00');
                          if($current_time>$T10_2){
                                $color='redbox';
                                $icon='cancel';
                              }else{ $color='yellowbox'; };
                      break;

                      default:
                          $color='yellowbox';
                          $icon='cancel';
                      break;
                  }
              }

              elseif(sizeof($pm_attendance)>$night_officer[$i]){
                  $color='bluebox';
                  $icon='add_circle';
              }

              elseif(sizeof($pm_attendance)==$night_officer[$i]){
              $color='greenbox';
              $icon='check_circle';
              
              }
          }

//////////////////////////////////////////////////// End of Night shift ////////////////////////////////////////

          if($_SERVER['PHP_SELF']=='/irep/dashboard.php'){
            $layout=" ";
            $end_tag=" ";
          }
          else{
            $layout="<div class='col s12 m12 l3'>";
            $end_tag="</div>";
          }
//////////////////////////////////////////////////// Container Box /////////////////////////////////////////        
?>

          <?= $layout ?>
            <div id="<?= $color ?>" class="<?= $animation ?>">
              <table>

                  <tr>
                      <td class="center">
                        <a href="#modaleastd<?= $i ?>" class="modal-trigger">
                        <p id="date" class="center">[Day]</p>
                        <span class="num">
                        <?php
                          if($siteid!='3282'){
                            echo sizeof($am_attendance)."/".$day_officer[$i];
                          }
                          else{
                            if(sizeof($am_attendance)<=$day_officer[$i]){
                            echo (sizeof($am_attendance))."/".$day_officer[$i];
                            }
                            else{
                             echo (sizeof($am_attendance)-1)."/".$day_officer[$i]; 
                            }
                          }
                        ?>
                        </span>
                        </a>
                      </td>
                      <?php 
                        if($ems=='Yes'){
                      ?>
                          <td class="center">
                            <a href="#modaleastmid<?= $i ?>" class="modal-trigger">
                            <p id="date" class="center">[Mid-Shift]</p>
                            <span class="num">
                            <?php
                              if($time>date('Y-m-d 09:30:00')){
                                echo sizeof($mid_attendance)."/".$emo;
                              }
                            ?>
                            </span>
                            </a>
                          </td>
                      <?php } ?>

                      <td class="center">
                        <a href="#modaleastn<?= $i ?>" class="modal-trigger">
                        <p id="date" class="center">[Night]</p>
                        <span class="num">
                        <?php
                            echo sizeof($pm_attendance)."/".$night_officer[$i];
                        ?>
                        </span>
                        </a>
                      </td>
                  </tr>

                  <tr>
                      <td colspan="3" class="center" id="sitename"><?= $east_site_list[$i]." [".$east_site_syn[$i]."]" ?></td>
                  </tr>
              </table>
            </div><br>
          <?= $end_tag ?>
<?php ///////////////////////////////////////////////////////////   End of box ////////////////////////////////////// ?>


    <div id="modaleastd<?= $i ?>" class="modal bottom-sheet">
        <div class="modal-content">
          <h5><?= $east_site_list[$i] ?> [Attendance Status] - <?= $phone[$i] ?> (Guard House Phone)</h5>

          <div class="col s12 m12 l12">
            <table id="data" class="bordered highlight">
              <tr>
                  <th class="center">Photo</th>
                  <th class="center">NRIC</th>
                  <th class="center">Name</th>
                  <th class="center">Attendance Time</th>
              </tr>
              <?php

              if(sizeof($am_attendance)==0){
              ?>
                  <tr>
                      <td class="center" colspan="4"><h4 class="red-text">Sorry! No Data To Show</h4></td>
                  </tr>
              <?php
              }

              if($siteid!='3282'){
                for($ii=0;$ii<sizeof($am_attendance);$ii++){ ?>
                          <tr>
                              <td class="center"><img src="http://ireppro.com/PhotoAttendance/<?= $photo[$ii] ?>" width="150" height="170" ></td>
                              <td class="center"><?= $nric[$ii] ?></td>
                              <td class="center"><?= $name[$ii] ?></td>
                              <td class="center"><?= $time[$ii] ?></td>
                          </tr>
              <?php
                  }
              }

              else{

                for($ii=0;$ii<sizeof($am_attendance);$ii++){
                  if($time[$ii]<date('Y-m-d 09:29:59')){
            ?>
                        <tr>
                            <td class="center"><img src="http://ireppro.com/PhotoAttendance/<?= $photo[$ii] ?>" width="150" height="170" ></td>
                            <td class="center"><?= $nric[$ii] ?></td>
                            <td class="center"><?= $name[$ii] ?></td>
                            <td class="center"><?= $time[$ii] ?></td>
                        </tr>        
            <?php        
                  }
                }
              }
            ?>
          </table>
        </div>
      </div>

      <div class="modal-footer">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn red">Close</a>
      </div>
    </div>

<?php ///////////////////////////////////////////  Day Modal End //////////////////////////////////////////////// ?>

    <div id="modaleastmid<?= $i ?>" class="modal bottom-sheet">

          <div class="modal-content">
            <h5><?= $east_site_list[$i] ?> [Attendance Status] - <?= $phone[$i] ?> (Guard House Phone)</h5>

            <div class="col s12 m12 l12">
              <table id="data" class="bordered highlight">
                <tr>
                    <th class="center">Photo</th>
                    <th class="center">NRIC</th>
                    <th class="center">Name</th>
                    <th class="center">Attendance Time</th>
                </tr>
              <?php
                if(sizeof($pm_attendance)==0){
              ?>
                <tr>
                    <td class="center" colspan="4"><h4 class="red-text  ">Sorry! No Data To Show</h4></td>
                </tr>
              <?php
                }
              for($ii=0;$ii<sizeof($mid_attendance);$ii++){ ?>
                <tr>
                    <td class="center"><img src="http://ireppro.com/PhotoAttendance/<?= $midphoto[$ii] ?>" width="150" height="170" ></td>
                    <td class="center"><?= $midnric[$ii] ?></td>
                    <td class="center"><?= $midname[$ii] ?></td>
                    <td class="center"><?= $midtime[$ii] ?></td>
                </tr>
            <?php
              }
            ?>
            </table>
          </div>
        </div>

        <div class="modal-footer">
          <a href="#!" class="modal-action modal-close waves-effect waves-green btn red">Close</a>
        </div>
    </div>

<?php ////////////////////////////////////////////////// End of Mid Shift Modal ///////////////////////////////////////////////////// ?>

    <div id="modaleastn<?= $i ?>" class="modal bottom-sheet">

          <div class="modal-content">
            <h5><?= $east_site_list[$i] ?> [Attendance Status] - <?= $phone[$i] ?> (Guard House Phone)</h5>

            <div class="col s12 m12 l12">
              <table id="data" class="bordered highlight">
                <tr>
                    <th class="center">Photo</th>
                    <th class="center">NRIC</th>
                    <th class="center">Name</th>
                    <th class="center">Attendance Time</th>
                </tr>
              <?php
                if(sizeof($pm_attendance)==0){
              ?>
                <tr>
                    <td class="center" colspan="4"><h4 class="red-text  ">Sorry! No Data To Show</h4></td>
                </tr>
              <?php
                }
              for($ii=0;$ii<sizeof($pm_attendance);$ii++){ ?>
                <tr>
                    <td class="center"><img src="http://ireppro.com/PhotoAttendance/<?= $photo_night[$ii] ?>" width="150" height="170" ></td>
                    <td class="center"><?= $nric_night[$ii] ?></td>
                    <td class="center"><?= $name_night[$ii] ?></td>
                    <td class="center"><?= $time_night[$ii] ?></td>
                </tr>
            <?php
              }
            ?>
            </table>
          </div>
        </div>

        <div class="modal-footer">
          <a href="#!" class="modal-action modal-close waves-effect waves-green btn red">Close</a>
        </div>
    </div>

<?php } ?>
