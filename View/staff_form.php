<form action="oemove.php" method="post">

  <div class="row">
      <div class="input-field col s12 m12 l4 offset-l4 center-align">
          <i class="material-icons prefix">search</i>
          <input id="icon_prefix2" name="datefilter" type="text" class="datepicker" class="validate">
          <label for="icon_prefix2">Search Date</label>
      </div>
  </div>

  <div class="row">
    <div class="col s12 m12 l4 offset-l4 center-align">
      <button class="btn btn-medium waves-effect waves-light" type="submit" name="filter" id="searchbtn">Filter
          <i class="material-icons right">search</i>
      </button>
      
    </div>
  </div>

  </form>