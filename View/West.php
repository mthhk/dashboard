<?php

  $cluster=Array_Data('site','site_id','West','site_cluster');
  $day_officer=Array_Data('site','site_cluster','West','day_officer');
  $night_officer=Array_Data('site','site_cluster','West','night_officer');
  $num_west_site=Array_Data('site','site_cluster','West','site_id');
  $west_site_list=Array_Data('site','site_cluster','West','site_name');
  $west_site_syn=Array_Data('site','site_cluster','West','site_syn');
  $timeslot=Array_Data('site','site_cluster','West','timeslot_id');
  $Phone=Array_Data('site','site_cluster','West','Phone');

  for($i=0;$i<sizeof($num_west_site);$i++){

      $siteid=$num_west_site[$i];

      $time1=date('Y-m-d 04:00:00');//Day
      $time2=date('Y-m-d 16:59:59');//Day

      if($current_time>=date('Y-m-d 17:00:00') && $current_time<=date('Y-m-d 23:59:59')){
        $time3=date('Y-m-d 17:00:00'); //night
        $time4=date('Y-m-d 03:59:59', strtotime('+1 day')); //night
      }
      else{
        $time3=date('Y-m-d 17:00:00', strtotime('-1 day')); //night
        $time4=date('Y-m-d 03:59:59'); //night
      }

      $am_attendance=Check_In_Out('cin',$siteid,'atime',$time1,$time2,'attendance_id');//table, condition 1, condition 2, Time 1, Time 2, Pull datalist
      $pm_attendance=Check_In_Out('cin',$siteid,'atime',$time3,$time4,'attendance_id');

      $time=Check_In_Out('cin',$siteid,'atime',$time1,$time2,'atime');
      $nric=Check_In_Out('cin',$siteid,'atime',$time1,$time2,'nric');
      $photo=Check_In_Out('cin',$siteid,'atime',$time1,$time2,'photo');
      $name=Check_In_Out('cin',$siteid,'atime',$time1,$time2,'fullname');

      $time_night=Check_In_Out('cin',$siteid,'atime',$time3,$time4,'atime');
      $nric_night=Check_In_Out('cin',$siteid,'atime',$time3,$time4,'nric');
      $photo_night=Check_In_Out('cin',$siteid,'atime',$time3,$time4,'photo');
      $name_night=Check_In_Out('cin',$siteid,'atime',$time3,$time4,'fullname');

      $T1_1=date('Y-m-d 06:00:00');
      $T2_1=date('Y-m-d 07:00:00');
      $T3_1=date('Y-m-d 07:30:00');
      $T4_1=date('Y-m-d 08:00:00');
      $T7_1=date('Y-m-d 08:45:00');
      $T8_1=date('Y-m-d 20:00:00');
      $T9_1=date('Y-m-d 07:00:00');
      $T10_1=date('Y-m-d 06:30:00');

      $T1_2=date('Y-m-d 18:00:00');
      $T2_2=date('Y-m-d 19:00:00');
      $T3_2=date('Y-m-d 19:30:00');
      $T4_2=date('Y-m-d 20:00:00');
      $T9_2=date('Y-m-d 22:00:00');
      $T10_2=date('Y-m-d 18:00:00');

      $current_time=date('Y-m-d H:i:s');

      Update_Officer($siteid,'today_officer',sizeof($am_attendance));
      Update_Officer($siteid,'tonight_officer',sizeof($pm_attendance));

    if($current_time>=$timing1 && $current_time<=$timing2){
      if(sizeof($pm_attendance)>=1){
        for($ii=0;$ii<sizeof($time_night);$ii++){

          $t1=strtotime($time_night[$ii]);
          $t2=strtotime($current_time);
          $total_west=round(($t2-$t1)/3600,2);
          //print_r($total_west);

            if($total_west>12){
              $animation='animated infinite fadeIn';
            }
            else{
              $animation=' ';
            }

        }
      }
      else{
            $animation=' ';
      }

     if(sizeof($am_attendance)<$day_officer[$i]){
          switch ($timeslot[$i]) {
            case 'T1':
            case 'T2':
            case 'T3':
            case 'T4':
            case 'T7':
            case 'T8':
            case 'T9':
            case 'T10':
              if($current_time>$T1_1 || $current_time>$T2_1 || $current_time>$T3_1 || $current_time>$T4_1 || $current_time>$T7_1 || $current_time>$T8_1 || $current_time>$T9_1 || $current_time>$T10_1){
                $color='redbox';
                $icon='cancel';
                // $animation='animated infinite fadeIn';
              }
              else{
                $color='yellowbox';
                $icon='remove_circle';
                // $animation='animated infinite fadeIn';
              };
              break;

            // case 'T3':
            //   if($current_time>$T3_1){
            //     $color='redbox';
            //     $icon='cancel';
            //     // $animation='animated infinite fadeIn';
            //   }
            //   else{
            //     $color='yellowbox';
            //     $icon='remove_circle';
            //     // $animation='animated infinite fadeIn';
            //   };
            //   break;

            // case 'T2':
            //   if($current_time>$T2_1){
            //     $color='redbox';
            //     $icon='cancel';
            //     // $animation='animated infinite fadeIn';
            //   }
            //   else{
            //     $color='yellowbox';
            //     $icon='remove_circle';
            //     // $animation='animated infinite fadeIn';
            //   };
            //   break;
          }
      }
      elseif(sizeof($am_attendance)>$day_officer[$i]){
          $color='bluebox';
          $icon='add_circle';
          // $animation='';
      }
      elseif(sizeof($am_attendance)==$day_officer[$i]){
          $color='greenbox';
          $icon='check_circle';
          // $animation='';
        }
  }

  else{

    if(sizeof($am_attendance)>=1){
      for($ii=0;$ii<sizeof($time);$ii++){

        $t1=strtotime($time[$ii]);
        $t2=strtotime($current_time);
        $total_west=round(($t2-$t1)/3600,2);

          if($total_west>12){
            $animation='animated infinite fadeIn';
          }
          else{
            $animation=' ';
          }

      }
    }
    else{
          $animation=' ';
    }

   if(sizeof($pm_attendance)<$night_officer[$i]){
        switch ($timeslot[$i]) {
          case 'T1':
          case 'T2':
          case 'T3':
          case 'T4':
          case 'T7':
          case 'T8':
          case 'T9':
          case 'T10':
              if($current_time>$T1_2 || $current_time>$T2_2 || $current_time>$T3_2 || $current_time>$T4_2 || $current_time>$T9_2 || $current_time>$T10_2){
              $color='redbox';
              $icon='cancel';
              // $animation='animated infinite fadeIn';
            }
            else{
              $color='yellowbox';
              $icon='remove_circle';
              // $animation='animated infinite fadeIn';
            };
            break;

          // case 'T3':
          //   if($current_time>$T3_2){
          //     $color='redbox';
          //     $icon='cancel';
          //     // $animation='animated infinite fadeIn';
          //   }
          //   else{
          //     $color='yellowbox';
          //     $icon='remove_circle';
          //     // $animation='animated infinite fadeIn';
          //   };
          //   break;

          // case 'T2':
          //   if($current_time>$T2_2){
          //     $color='redbox';
          //     $icon='cancel';
          //     // $animation='animated infinite fadeIn';
          //   }
          //   else{
          //     $color='yellowbox';
          //     $icon='remove_circle';
          //     // $animation='animated infinite fadeIn';
          //   };
          //   break;
        }
    }
    elseif(sizeof($pm_attendance)>$night_officer[$i]){
        $color='bluebox';
        $icon='add_circle';
        // $animation='';
    }
    elseif(sizeof($pm_attendance)==$night_officer[$i]){
        $color='greenbox';
        $icon='check_circle';
        // $animation='';
      }
}

if($_SERVER['PHP_SELF']=='/irep/dashboard.php'){
  $layout=" ";
  $end_tag=" ";
}
else{
  $layout="<div class='col s12 m12 l3'>";
  $end_tag="</div>";
}

?>

<?= $layout ?>

<div id="<?= $color ?>" class="<?= $animation ?>">
  <table>
      <tr>

          <td class="center">
            <a href="#modalwestd<?= $i ?>" class="modal-trigger">
            <p id="date" class="center">[Day]</p>
            <span class="num">
            <?php
                echo sizeof($am_attendance)."/".$day_officer[$i];
            ?>
            </span>
            </a>
          </td>

          <td class="center">
            <a href="#modalwestn<?= $i ?>" class="modal-trigger">
            <p id="date" class="center">[Night]</p>
            <span class="num">
            <?php
                echo sizeof($pm_attendance)."/".$night_officer[$i];
            ?>
            </span>
            </a>
          </td>
      </tr>
      <tr>
          <td colspan="3" class="center" id="sitename"><?= $west_site_list[$i]." [".$west_site_syn[$i]."]" ?></td>
      </tr>
  </table>
</div><br>

<?= $end_tag ?>


<div id="modalwestd<?= $i ?>" class="modal bottom-sheet">
  <div class="modal-content">
    <h5><?= $west_site_list[$i] ?> [Attendance Status] - <?= $Phone[$i] ?> (Guard House Phone)</h5>

    <div class="col s12 m12 l12">
      <table id="data" class="bordered highlight">
        <tr>
            <th class="center">Photo</th>
            <th class="center">NRIC</th>
            <th class="center">Name</th>
            <th class="center">Attendance Time</th>
        </tr>
<?php

if(sizeof($am_attendance)==0){
?>
        <tr>
            <td class="center" colspan="4"><h4 class="red-text">Sorry! No Data To Show</h4></td>
        </tr>
<?php
}
for($ii=0;$ii<sizeof($am_attendance);$ii++){ ?>
        <tr>
            <td class="center"><img src="http://ireppro.com/PhotoAttendance/<?= $photo[$ii] ?>" width="150" height="170" ></td>
            <td class="center"><?= $nric[$ii] ?></td>
            <td class="center"><?= $name[$ii] ?></td>
            <td class="center"><?= $time[$ii] ?></td>
        </tr>
<?php
}
?>
    </table>
  </div>

</div>
<div class="modal-footer">
  <a href="#!" class="modal-action modal-close waves-effect waves-green btn red">Close</a>
</div>
</div>


<div id="modalwestn<?= $i ?>" class="modal bottom-sheet">
  <div class="modal-content">
    <h5><?= $west_site_list[$i] ?> [Attendance Status] - <?= $phone[$i] ?> (Guard House Phone)</h5>

    <div class="col s12 m12 l12">
      <table id="data" class="bordered highlight">
        <tr>
            <th class="center">Photo</th>
            <th class="center">NRIC</th>
            <th class="center">Name</th>
            <th class="center">Attendance Time</th>
        </tr>
<?php
if(sizeof($pm_attendance)==0){
?>
  <tr>
      <td class="center" colspan="4"><h4 class="red-text  ">Sorry! No Data To Show</h4></td>
  </tr>
<?php
}
for($ii=0;$ii<sizeof($pm_attendance);$ii++){ ?>
<tr>
    <td class="center"><img src="http://ireppro.com/PhotoAttendance/<?= $photo_night[$ii] ?>" width="150" height="170" ></td>
    <td class="center"><?= $nric_night[$ii] ?></td>
    <td class="center"><?= $name_night[$ii] ?></td>
    <td class="center"><?= $time_night[$ii] ?></td>
</tr>
<?php
}
?>
    </table>
  </div>

</div>
<div class="modal-footer">
  <a href="#!" class="modal-action modal-close waves-effect waves-green btn red">Close</a>
</div>
</div>

<?php } ?>
