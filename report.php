<?php
include("Controller/api.php");
include("Controller/db.php");
date_default_timezone_set("Asia/Singapore");

if($_SESSION['role']=="client"){
  echo header("location:client.php");
}
if(!isset($_SESSION['email'])){
	echo header("location:index.php");
}
else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <title>Report</title>
  <?php include("lib/materialize.php"); ?>

</head>
<body>

 	<div class="row">
	 	<?php include("lib/nav.php"); ?>
 	</div>

 	<div class="row">
 		<?php include("View/attendance-report.php"); ?>
 	</div>

 	<div class="row">
 		<?php include("View/attendance-table.php"); ?>
 	</div>


<?php include("lib/js.php"); ?>
</body>
</html>
<?php } ?>
