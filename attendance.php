<?php
include("lib/materialize.php");
include("Controller/api.php");
include("Controller/db.php");
date_default_timezone_set("Asia/Singapore");

if($_SESSION['role']=="client"){
  echo header("location:client.php");
}

if(!isset($_SESSION['email'])){
  echo header("location:index.php");
}
else{
  $page=$_SERVER['PHP_SELF'];
  $sec="300";
  $time1=date('Y-m-d 04:00:00');
  $time2=date('Y-m-d 12:00:00');
  $time3=date('Y-m-d 16:00:00');
  $time4=date('Y-m-d 23:59:59');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'">
  <title>MSS Dashboard</title>

</head>
<body>

 	<div class="row">
	 	<?php include("lib/nav.php"); ?>
    <div class="progress">
      <div class="indeterminate"></div>
    </div>

 	</div>

  <div class="row">


     <h4 class="center">Filter</h4>

 	</div>

  <!-- <div class="row">

    <div class="col s12 m12 l2">

    </div>

    <div class="col s12 m12 l2">
      <input name="group1" type="radio" id="day" checked />
      <label for="day" class="cbox">Day Shift</label>
      <input name="group1" type="radio" id="night" />
      <label for="night" class="cbox">Night Shift</label>
    </div>

    <div class="col s12 m12 l4 center">
      <input type="checkbox" id="east"/>
      <label for="east" class="cbox">EAST </label>
      <input type="checkbox" id="west"/>
      <label for="west" class="cbox">WEST </label>
      <input type="checkbox" id="north"/>
      <label for="north" class="cbox">NORTH </label>
      <input type="checkbox" id="central"/>
      <label for="central" class="cbox">CENTRAL </label>


    </div>

    <div class="col s12 m12 l2">

    </div>

  </div> -->

 	<div class="row">
 		<div class="col s12 m12 l2 offset-l2">
 			 <div id="east">
          <h5 class="center">EAST</h5>
          <?php include_once('View/modal-east.php'); ?>
          <?php print_r($a) ?>
			  </div>
 		</div>

    <div class="col s12 m12 l2">
 			 <div id="west">
          <h5 class="center">WEST</h5>
			 </div>
 		</div>

    <div class="col s12 m12 l2">
 			 <div id="north">
          <h5 class="center">NORTH</h5>
			 </div>
 		</div>

    <div class="col s12 m12 l2">
 			 <div id="central">
          <h5 class="center">CENTRAL</h5>
			  </div>
 		</div>

    <div class="col s12 m12 l2"></div>
  </div>
 <!-- Cluster Column -->
<div class="row">


  <div class="col s12 m12 l2 offset-l2">
    <?php include('View/East.php'); ?>
  </div>

  <!-- <div class="col s12 m12 l2 l2">
    <?php include('View/West.php'); ?>
  </div> -->

   <!-- <div class="col s12 m12 l2 l2">
    <?php include('View/North.php'); ?>
  </div>

  <div class="col s12 m12 l2 l2">
    <?php include('View/North.php'); ?>
  </div> -->




</div>

<?php include("lib/js.php"); ?>
</body>
</html>
<?php  } ?>
