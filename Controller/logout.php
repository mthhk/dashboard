<?php
include_once('db.php');
session_start();

if(isset($_SESSION['email']))
{
unset($_SESSION['email']);
unset($_SESSION['role']);
unset($_SESSION['userid']);
unset($_SESSION['siteid']);

echo header("location:/irep/index.php");
}

?>