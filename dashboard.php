<?php
include("lib/materialize.php");
include("Controller/api.php");
include("Controller/db.php");
date_default_timezone_set("Asia/Singapore");

if(!isset($_SESSION['email'])){
  echo header("location:index.php");
}
if($_SESSION['role']=='client'){
    if($_SESSION['userid']=='29'){
      echo header("location:clockChart.php");  
    }
    else{
      echo header("location:client.php");
    }
}
else{

  $page=$_SERVER['PHP_SELF'];
  $sec="300";
  $current_time=date('Y-m-d H:i:s');
  $timing1=date('Y-m-d 04:00:00');
  $timing2=date('Y-m-d 16:59:59');
  $dump_time=date('Y-m-d 03:59:59');//Day

  $time1=date('Y-m-d 04:00:00');//Day
  $time2=date('Y-m-d 16:59:59');//Day

  $time3=date('Y-m-d 17:00:00');//Night
  $time4=date('Y-m-d 04:59:59');//Night

  $time5=date('Y-m-d 23:59:59', strtotime('-1 day'));


?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'">
  <link rel="icon" href="image/logoicon.png">
  <title>MSS Attendance</title>
  <script src="http://code.responsivevoice.org/responsivevoice.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script>
      var text="<?php echo "Welcome to Metropolis Security Systems"; ?>";
      responsiveVoice.speak(text, "Fallback UK Female", {pitch: 1.0,rate:0.8});
  </script>
  <script type="text/javascript">
    $(document).ready(function(){
      // alert("Hello");
        // $.get("View/East.php",function(data,status){
        //   $("#attendance").html(data);
        // });
      
    });
  </script>
  <?php include("lib/react.php"); ?>
</head>
<body>

 	<div class="row">
	 	<?php include("lib/nav.php"); ?>
    <?php	include("test.php"); ?>
    <div class="progress">
      <div class="indeterminate"></div>
    </div>
 	</div>

  <div class="row">
     <h2 class="center mss">Attendance Status <span class="white-text" id="time"> </span></h2>
  </div>

  <?php include('View/wrongic.php'); ?>
  <?php include('View/ot.php'); ?>
  <?php include('View/otdata.php'); ?>

  <!-- <div class="row">
      <div class="col s12 m12 l3">
        <div class="progress">
          <div class="determinate" id="day" style="width: 40%"></div>
        </div>
        <div class="progress">
          <div class="determinate" id="total" style="width: 100%"></div>
        </div>
        <div class="progress">
          <div class="determinate" id="night" style="width: 70%"></div>
        </div>
      </div>
  </div> -->

<!-- //////////////////////////////////////////////////////  Data  ////////////////////////////////////////////// -->
  <div class="row" id="all">
    <div class="col s12 m12 l3">

      <div class="row center">
        <?php
          if(!empty($east_wrong)){
        ?>
        <a href="#eastwrongic" class="btn btn-floating orange darken-1 pulse modal-trigger"><i class="material-icons">priority_high</i></a>
        <?php
        }
        else{
        ?>
        <a href="#eastwrongic" class="btn btn-floating orange darken-1 disabled"><i class="material-icons">priority_high</i></a>
        <?php
        }
        if($east==true){
        ?>
        <a href="#oteast" class="btn btn-floating light-blue accent-3 pulse modal-trigger"><i class="material-icons">add_alarm</i></a>
        <?php
        }
        else{
        ?>
        <a href="#oteast" class="btn btn-floating light-blue accent-3 disabled"><i class="material-icons">add_alarm</i></a>
        <?php } ?>

        <?php
        if($eastn==true){
        ?>
        <a href="#eastn" class="btn btn-floating teal lighten-1 pulse modal-trigger"><i class="material-icons">access_alarm</i></a>
        <?php } 
        else{
        ?>
        <a href="#eastn" class="btn btn-floating light-blue accent-3 disabled"><i class="material-icons">access_alarm</i></a>
        <?php
        }
        ?>
      </div>

 			 <div class="row" id="east">
          <h5 class="center">EAST</h5>
			  </div>

        <div class="row" id="attendance">
            <?php include('View/East.php'); ?>
        </div>
 		</div>

    <div class="col s12 m12 l3">
      <div class="row center">
          <?php
          if(!empty($central_wrong)){
          ?>
          <a href="#centralwrongic" class="btn btn-floating  orange darken-1 pulse modal-trigger"><i class="material-icons">priority_high</i></a>
        <?php  } else{  ?>
          <a href="#centralwrongic" class="btn btn-floating  orange darken-1 disabled"><i class="material-icons">priority_high</i></a>
          <?php
          }
          if($central==true){
          ?>
          <a href="#otcentral" class="btn btn-floating  light-blue accent-3 pulse modal-trigger"><i class="material-icons">add_alarm</i></a>
        <?php } else{ ?>
          <a href="#otcentral" class="btn btn-floating  light-blue accent-3 disabled"><i class="material-icons">add_alarm</i></a>
        <?php } ?>
        <?php
        if($centraln==true){
        ?>
        <a href="#centraln" class="btn btn-floating  teal lighten-1 pulse modal-trigger"><i class="material-icons">access_alarm</i></a>
        <?php } 
        else{
        ?>
        <a href="#centraln" class="btn btn-floating light-blue accent-3 disabled"><i class="material-icons">access_alarm</i></a>
        <?php
        }
        ?>
      </div>

       <div class="row" id="central">
          <h5 class="center">CENTRAL</h5>
        </div>

        <div class="row">
            <?php include('View/Central.php'); ?>
        </div>
    </div>

    <div class="col s12 m12 l3">
      <div class="row center">
          <?php
            if(!empty($west_wrong)){
          ?>
          <a href="#westwrongic" class="btn btn-floating orange darken-1 pulse modal-trigger"><i class="material-icons">priority_high</i></a>
          <?php
            }
            else{
          ?>
          <a href="#westwrongic" class="btn btn-floating disabled"><i class="material-icons">priority_high</i></a>
          <?php
            }
          ?>
          <?php
          if($west==true){
          ?>
          <a href="#otwest" class="btn btn-floating light-blue accent-3 pulse modal-trigger"><i class="material-icons">add_alarm</i></a>
        <?php
          }
          else{
        ?>
        <a href="#otwest" class="btn btn-floating disabled"><i class="material-icons">add_alarm</i></a>
        <?php
        }
        ?>
        <?php
        if($westn==true){
        ?>
        <a href="#westn" class="btn btn-floating teal lighten-1 pulse modal-trigger"><i class="material-icons">access_alarm</i></a>
        <?php } 
        else{
        ?>
        <a href="#westn" class="btn btn-floating light-blue accent-3 disabled"><i class="material-icons">access_alarm</i></a>
        <?php
        }
        ?>
      </div>

 			 <div class="row" id="west">
          <h5 class="center">WEST</h5>
			 </div>

       <div class="row">
           <?php include('View/West.php'); ?>
       </div>
 		</div>

    <div class="col s12 m12 l3">
      <div class="row center">
          <?php
            if(!empty($north_wrong)){
          ?>
          <a href="#northwrongic" class="btn btn-floating orange darken-1 pulse modal-trigger"><i class="material-icons">priority_high</i></a>
          <?php
            }
            else{
          ?>
          <a href="#northwrongic" class="btn btn-floating orange darken-1 disabled"><i class="material-icons">priority_high</i></a>
          <?php
          }
          if($north==true){
          ?>
          <a href="#otnorth" class="btn btn-floating light-blue accent-3 pulse modal-trigger"><i class="material-icons">add_alarm</i></a>
        <?php } else{ ?>
          <a href="#otnorth" class="btn btn-floating light-blue accent-3 disabled"><i class="material-icons">add_alarm</i></a>
        <?php } ?>
        <?php
        if($northn==true){
        ?>
        <a href="#northn" class="btn btn-floating teal lighten-1 pulse modal-trigger"><i class="material-icons">access_alarm</i></a>
        <?php } 
        else{
        ?>
        <a href="#northn" class="btn btn-floating light-blue accent-3 disabled"><i class="material-icons">access_alarm</i></a>
        <?php
        }
        ?>
      </div>

 			 <div class="row" id="north">
          <h5 class="center">NORTH</h5>
			 </div>
       <div class="row">
           <?php include('View/North.php'); ?>
       </div>
 		</div>
  </div>


<?php include("lib/js.php"); ?>
<?php include("materialize/app.js"); ?>
</body>
</html>
<?php  } ?>
