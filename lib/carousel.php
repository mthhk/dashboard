<div class="slider z-depth-4">
    <ul class="slides indigo darken-1">
      <li>
        <div class="caption center-align">
          <h3 class="title">INS Technology</h3>
        </div>
      </li>
      <li>
        <div class="caption left-align">
          <h3 class="title">Rapid Development</h3>
        </div>
      </li>
  	  <li>
        <div class="caption right-align">
          <h3 class="title">Resonable Cost</h3>
        </div>
      </li>
      <li>
        <div class="caption center-align">
          <h3 class="title">We Are Ready For You</h3>
        </div>
      </li>
      <li>
        <div class="caption left-align">
          <h3 class="title">Call Us Now</h3>
        </div>
      </li>
    </ul>
</div>
