<div class="navbar-fixed">

  <ul id="dashboard" class="dropdown-content">
    <li><a href="dashboard.php"><i class="material-icons left">assignment</i>Attendance</a></li>
    <li><a href="guardtour.php"><i class="material-icons left">data_usage</i>Clocking</a></li>
    <li><a href="clockChart.php"><i class="material-icons left">show_chart</i>Charts</a></li>
  </ul>

  
  <ul id="cluster" class="dropdown-content">
    <li><a href="dashboard.php">All Clusters</a></li>
    <li><a href="East.php">East</a></li>
    <li><a href="West.php">West</a></li>
    <li><a href="North.php">North</a></li>
    <li><a href="Central.php">Central</a></li>
    <li><a href="oemove.php">Site Movement</a></li>
    <?php
    if($_SESSION['role']=="admin"){
    ?>
    <li><a href="staff.php">Management Staff</a></li>
    <?php  
    }
    ?>
  </ul>

  <ul id="report" class="dropdown-content">
    <li><a href="report.php">Attendance</a></li>
    <li><a href="clock-report.php">Clocking</a></li>
  </ul>

  <ul id="logout" class="dropdown-content">
    <!-- <li><a href="sites.php"><i class="material-icons left">add_box</i>Add Site</a></li> -->
    <!-- <li><a href="help.php"><i class="material-icons left">help</i>Help</a></li> -->
    <!-- <li><a href="profile.php"><i class="material-icons left">account_box</i>Update Password</a></li> -->
    <li><a href="/irep/Controller/logout.php"><i class="material-icons left">chevron_left</i>Logout</a></li>
  <?php if($_SESSION['role']=="admin"){ ?>
    <li><a href="/irep/Controller/setup.php"><i class="material-icons left">build</i>DB Set Up</a></li>
    <!-- <li><a href="/irep/Controller/clocking.php"><i class="material-icons left">build</i>try</a></li> -->
  <?php } ?>
  </ul>

    <nav class="z-depth-3">
        <div class="nav-wrapper">

          <a href="<?php $_SERVER['PHP_SELF'] ?>" class="brand-logo left" id="header">MSS Dashboard</a>
          <a href="#" data-activates="mobile-demo" class="button-collapse right"><i class="material-icons">menu</i></a>
          <ul class="right hide-on-med-and-down">
            <li><a class="dropdown-button" href="#!" data-activates="dashboard">Real-Time Dashboard<i class="material-icons right" id="icon">arrow_drop_down</i></a></li>
            <li><a class="dropdown-button" href="#!" data-activates="cluster">View By Cluster<i class="material-icons right" id="icon">arrow_drop_down</i></a></li>
            <li><a class="dropdown-button" href="#!" data-activates="report">View Report<i class="material-icons right" id="icon">arrow_drop_down</i></a></li>
            <li><a class="dropdown-button" href="#!" data-activates="logout"><?= $_SESSION['email'] ?><i class="material-icons right" id="icon">arrow_drop_down</i></a></li>
          </ul>


      </div>
    </nav>
</div>


<ul class="side-nav" id="mobile-demo">
  <li><a href="dashboard.php">Attendance</a></li>
  <li><a href="guardtour.php">Clocking</a></li>
  <li><a href="clockChart.php">Charts</a></li>
  <li><a href="/irep/Controller/logout.php">Logout</a></li>
</ul>