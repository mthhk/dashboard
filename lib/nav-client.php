<div class="navbar-fixed">

  <ul id="logout" class="dropdown-content">
    <!-- <li><a href="sites.php"><i class="material-icons left">add_box</i>Add Site</a></li> -->
    <!-- <li><a href="help.php"><i class="material-icons left">help</i>Help</a></li> -->
    <!-- <li><a href="profile.php"><i class="material-icons left">account_box</i>Update Password</a></li> -->
    <li><a href="/irep/Controller/logout.php"><i class="material-icons left">chevron_left</i>Logout</a></li>
  </ul>

    <nav class="z-depth-3">
        <div class="nav-wrapper">

          <a href="<?php $_SERVER['PHP_SELF'] ?>" class="brand-logo left" id="header">MSS Dashboard</a>
          <a href="#" data-activates="mobile-demo" class="button-collapse right"><i class="material-icons">menu</i></a>
          <ul class="right hide-on-med-and-down">
            <li><a class="dropdown-button" href="#!" data-activates="logout"><?= $_SESSION['email'] ?><i class="material-icons right" id="icon">arrow_drop_down</i></a></li>
          </ul>


      </div>
    </nav>
</div>


<ul class="side-nav" id="mobile-demo">
  <li><a href="#"><a><?= $_SESSION['email'] ?></a></li>
  <li><a href="/irep/Controller/logout.php">Logout</a></li>
</ul>
