<script>
      let chart1=document.getElementById('myBar');
      let chart2=document.getElementById('myLine');
      let chart3=document.getElementById('myPie');
      let chart4=document.getElementById('myPie2');
      let drawChart1=new Chart(chart1,{
        type:'bar',
        data:{
          labels:["<?= date('d-D',strtotime($todayDate.'-7 day')) ?>","<?= date('d-D',strtotime($todayDate.'-6 day')) ?>", "<?= date('d-D',strtotime($todayDate.'-5 day')) ?>", "<?= date('d-D',strtotime($todayDate.'-4 day')) ?>", "<?= date('d-D',strtotime($todayDate.'-3 day')) ?>", "<?= date('d-D',strtotime($todayDate.'-2 day')) ?>", "<?= date('d-D',strtotime($todayDate.'-1 day')) ?>"],
          datasets: [{
              label: '# of clocking',
              data: [<?= $day7.','.$day6.','.$day5.','.$day4.','.$day3.','.$day2.','.$day1 ?>],
              backgroundColor: [
                  'rgba(255, 50, 190, 0.5)',
                  'rgba(255, 99, 132, 0.5)',
                  'rgba(54, 162, 235, 0.5)',
                  'rgba(255, 206, 86, 0.5)',
                  'rgba(75, 192, 192, 0.5)',
                  'rgba(153, 102, 255, 0.5)',
                  'rgba(255, 159, 64, 0.5)'
              ],
              borderColor: [
                'rgba(255, 50, 190, 1)',
                  'rgba(255,99,132,1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(255, 159, 64, 1)'
              ],
              borderWidth: 0.9
          }]
        },
        options: {
          plugins:{
            labels:{
              render:'value',
              fontSize: 17,
              precision: 2,
            }
          },
          responsive: true,
          maintainAspectRatio: true,
          title:{
              display: true,
              text:'Frequency of Clocking ',
              position:'top',
              fontSize:20
          },
          legend:{
            position:'bottom'
          },
          scales: {
              yAxes: [{
                  ticks: {
                      min:0,
                      max:6,
                      beginAtZero:true
                  },
                  gridLines:{
                    offsetGridLines: false
                  }
              }]
          }
        }
      });

      let drawChart2=new Chart(chart2,{
        type:'line',
        data:{
            labels: ["<?= date('d-D',strtotime($todayDate.'-7 day')) ?>","<?= date('d-D',strtotime($todayDate.'-6 day')) ?>", "<?= date('d-D',strtotime($todayDate.'-5 day')) ?>", "<?= date('d-D',strtotime($todayDate.'-4 day')) ?>", "<?= date('d-D',strtotime($todayDate.'-3 day')) ?>", "<?= date('d-D',strtotime($todayDate.'-2 day')) ?>", "<?= date('d-D',strtotime($todayDate.'-1 day')) ?>"],
          datasets: [{
              label: 'Avg Clocking Time Spent (min) ',
              data: [<?php 
                $j=sizeof($dbdc)-1;
                while($j>=0){
                   switch ($j) {
                       case 0:
                        echo $dbdc[$j];   
                        break;
                   
                       default:
                        echo $dbdc[$j].",";
                        break;
                   }
                    $j--;
                } 
                    ?>],
              backgroundColor: [
                  'rgba(153, 102, 255, 0.5)'
              ],
              borderColor: [
                  'rgba(153, 102, 255, 1)',
              ],
              borderWidth: 0.6
          }]
        },
        options: {
          plugins:{
            labels:{
              render:'value',
              fontSize: 20,
              precision: 2,
            }
          },
          title:{
              display: true,
              text:'Average Time Spent on Clocking : <?= round(array_sum($dbdc)/sizeof($dbdc),2); ?> mins',
              position:'top',
              fontSize:20
          },
          legend:{
            position:'bottom'
          },
          scales: {
              yAxes: [{
                  ticks: {
                      min:0,
                      max:60,
                      beginAtZero:true
                  },
                  gridLines:{
                    offsetGridLines: false
                  }
              }]
          }
        }
      });

      let drawChart3=new Chart(chart3,{
        type:'doughnut',
        data:{
          labels: ['Incomplete Clocking','Complete Clocking'],
          datasets: [{
              label: 'Avg Clocking Time Spent (min) ',
              data: [<?= $percentIncomplete.",".$percentComplete ?>],
              backgroundColor: [
                  'rgba(247, 154, 255, 0.5)',
                  'rgba(75, 192, 192, 0.5)'
              ],
              borderColor: [
                  'rgba(255, 159, 64, 1)',
                  'rgba(75, 192, 192, 1)'
              ],
              borderWidth: 0.2
          }]
        },
        options: {
          plugins:{
            labels:{
              render:'value%',
              fontSize: 20,
              precision: 2,
            }
          },
          title:{
              display: true,
              text:'Clocking Completion Rate (%)',
              position:'top',
              fontSize:17
          },
          legend:{
            position:'bottom',
          }
        }
      });

      let drawChart4=new Chart(chart4,{
        type:'doughnut',
        data:{
          labels: ['Clocking Route Missed Point','Full Complete Clocking'],
          datasets: [{
              label: 'Avg Clocking Time Spent (min) ',
              data: [<?= sizeof($missed).",".sizeof($nomissed) ?>],
              backgroundColor: [
                  'rgb(249, 42, 42, 0.7)',
                  'rgb(98, 255, 177, 0.7)'
              ],
              borderColor: [
                  'rgb(249, 42, 42, 1)',
                  'rgb(255, 105, 63, 1)'
              ],
              borderWidth: 0.2
          }]
        },
        options: {
          plugins:{
            labels:{
              render:'value',
              fontSize: 20,
              precision: 2,
            }
          },
          title:{
              display: true,
              text:'Monthly Data - Num: of Missed & Completed Clocking Route',
              position:'top',
              fontSize:17
          },
          legend:{
            position:'bottom',
          }
        }
      });



      var options1 = {
        chart: {  
          height:160,    
          type: "radialBar",
        },
        series: [<?= $tdAvgTime; ?>],
        colors: ["#02d19d"],
        plotOptions: {
          radialBar: {
            size: 110,
            startAngle: -90,
            endAngle: 90,
            track: {
              background: '#02d19d',
              startAngle: -90,
              endAngle: 90,
              strokeWidth: '97%',
              margin: 5,
              opacity: 0.2
            },
            dataLabels: {
              name: {
                show: true,
                text:'Clocking'
              },
              value: {
                fontSize: "20px",
                color: '#000',
                show: true,
                
                formatter: function(val){
                  return 'Today - '+val + ' mins';
                }
              }
            }
          }
        },
        labels: ["Avg Time Spent"]
      };
      new ApexCharts(document.querySelector("#card"), options1).render();
      
      var options2 = {
        chart: {  
          height:160,    
          type: "radialBar",
        },
        series: [<?= round(array_sum($dbdc)/sizeof($dbdc),2); ?>],
        colors: ["#ad82ff"],
        plotOptions: {
          radialBar: {
            size: 110,
            startAngle: -90,
            endAngle: 90,
            track: {
              background: '#ad82ff',
              startAngle: -90,
              endAngle: 90,
              strokeWidth: '97%',
              margin: 5,
              opacity: 0.2
            },
            dataLabels: {
              name: {
                show: true,
                text:'Clocking'
              },
              value: {
                fontSize: "20px",
                color:  '#000',
                show: true,
                formatter: function(val){
                  return 'Last 7 Days - '+val + ' mins';
                }
              }
            }
          }
        },
        stroke: {
          lineCap: "butt"
        },
        labels: ["Avg Time Spent"]
      };
      new ApexCharts(document.querySelector("#card1"), options2).render();

      var options3 = {
        chart: {  
          height:160,    
          type: "radialBar",
        },
        series: [<?= $mthAvgTime; ?>],
        colors: ["#fc2aa5"],
        plotOptions: {
          radialBar: {
            size: 110,
            startAngle: -90,
            endAngle: 90,
            track: {
              background: '#fc2aa5',
              startAngle: -90,
              endAngle: 90,
              strokeWidth: '97%',
              margin: 5,
              opacity: 0.2
            },
            dataLabels: {
              name: {
                show: true,
                text:'Clocking'
              },
              value: {
                fontSize: "20px",
                color:  '#000',
                show: true,
                formatter: function(val){
                  return 'Last Month - '+val + ' mins';
                }
              }
            }
          }
        },
        stroke: {
          lineCap: "butt"
        },
        labels: ["Avg Time Spent"]
      };
      new ApexCharts(document.querySelector("#card2"), options3).render();

  </script>