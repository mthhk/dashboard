<script type="text/babel">
		function timeTick(){
			const element=(
			<div>
				<h5>{new Date().toLocaleTimeString()}</h5>
			</div>
			);
			ReactDOM.render(element,document.getElementById('time'));
		}
		setInterval(timeTick,1000);

		class Table extends React.Component{
			render(){
				return(
					<table>
						<thead>
							<tr>
								<th>Image</th>
								<th>Name</th>
								<th>ID</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Image</td>
								<td>Name</td>
								<td>ID</td>
							</tr>
							<tr>
								<td>Image</td>
								<td>Name</td>
								<td>ID</td>
							</tr>
						</tbody>
					</table>
				);
			}
		}
		ReactDOM.render(<Table />,document.getElementById('table'));

</script>


