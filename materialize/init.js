(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('.parallax').parallax();
    $('.slider').slider({'interval':2500,'indicators':false,'height':260});
    // $('.datepicker').pickadate({
    // 	'format': 'yyyy-mm-dd',
    // 	'closeOnSelect':true,
    // 	'selectMonths':true,
    // 	'selectYears':true
    // });
    $('select').material_select();
    $('.modal').modal();
    // $('#dateFrom').datepicker({
    //   'format': 'yyyy-mm-dd',
    // });
    $('.datepicker').datepicker();


  }); // end of document ready
})(jQuery);
