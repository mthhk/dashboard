<?php
include("Controller/api.php");
include("Controller/db.php");

if($_REQUEST['dateFrom'] || $_REQUEST['dateTo']){
    $dateFrom=$_REQUEST['dateFrom'];
    $dateTo=$_REQUEST['dateTo'];
    $ajaxData=clock_data2($_SESSION['siteid'],date('Y-m-d 00:00:00',strtotime($dateFrom)),date('Y-m-d 23:59:59',strtotime($dateTo)),'report_link','remarks','start_clock_time','end_clock_time','missed_checkpoint');

    for($i=0;$i<sizeof($ajaxData);$i++){
    echo "<tr>";
    echo "<td class='text-center'><a href='".$ajaxData[$i]['report_link']."' target='_blank' id='ajaxbtn'><i class='material-icons'>remove_red_eye</i></a></td>";
    echo "<td class='text-center'>".date('d-M H:i:s',strtotime($ajaxData[$i]['start_clock_time']))."</td>";
    echo "<td class='text-center'>".date('d-M H:i:s',strtotime($ajaxData[$i]['end_clock_time']))."</td>";
    if($ajaxData[$i]['missed_checkpoint']=="true"){
        echo "<td class='text-center'>YES - <a href='".$ajaxData[$i]['report_link']."' class='red-text' target='_blank' id='ajaxbtn'>View Report</a></td>";
        }
        else{
          echo " ";
        }
    echo "<td class='text-center'>".$ajaxData[$i]['remarks']."</td>";
    echo "</tr>";
    }
}

if($_REQUEST['pstartTime']){
    $dateFrom=$_REQUEST['pstartTime'];
    $dateTo=$_REQUEST['pendTime'];
    // $dateFrom=date('Y-m-11 00:00:00');
    // $dateTo=date('Y-m-11 23:59:59');
    $order="ORDER BY start_clock_time DESC";
    $ajaxData=clock_data2($dateFrom,$dateTo,'report_link','missed_checkpoint','end_clock_time',$order);
    
    for($i=0;$i<=sizeof($ajaxData)-1;$i++){
        if($ajaxData[$i]['missed_checkpoint']=="true"){
            echo "<div class='alert alert-danger alert-dismissible' id='alert'>";
            echo "<h6><strong>Alert!</strong> Missed Clocking Points - <strong>".date('h:i:s A',strtotime($ajaxData[$i]['end_clock_time']))." - </strong><a href=".$ajaxData[$i]['report_link']." target='_BLANK' class='alert-link'>See Report</a></h6>";
            echo "</div>";
        }else{
            echo " ";
        }
    }
    //echo "1";
}
?>