<?php
include("lib/materialize.php");
include("Controller/api.php");
include("Controller/db.php");
date_default_timezone_set("Asia/Singapore");

if($_SESSION['role']=="client"){
  echo header("location:client.php");
}

if(!isset($_SESSION['email'])){
  echo header("location:index.php");
}
else{
  $page=$_SERVER['PHP_SELF'];
  $sec="300";
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'">
  <link rel="icon" href="image/logoicon.png">
  <title>MSS Attendance</title>
  <script src="http://code.responsivevoice.org/responsivevoice.js"></script>
</head>
<body>

  <div class="row">
    <?php include("lib/nav.php"); ?>
    <?php include("test.php"); ?>
    <div class="progress">
      <div class="indeterminate"></div>
    </div>
  </div>

  <div class="row">
    <h3 class="center mss">Management Site Visit Log</h3>
    <?php
      if(!empty($_POST['datefilter'])){
        echo "<h4 class='center mss'>".$_POST['datefilter']."</h4>";
      }else{
        echo "<h4 class='center mss'>Live Data</h4>";
      }
    ?>
  </div>
 
  <div class="row">
    <?php include('View/staff_form.php'); ?>
  </div>

  <div class="row">
    <?php include('View/staff_table.php'); ?>
  </div>


<?php include("lib/js.php"); ?>
</body>
</html>
<?php } ?>
