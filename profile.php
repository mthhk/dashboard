<?php
include("lib/materialize.php");
include("Controller/api.php");
include("Controller/db.php");
date_default_timezone_set("Asia/Singapore");

if($_SESSION['role']=="client"){
  echo header("location:client.php");
}
if(!isset($_SESSION['email'])){
  echo header("location:index.php");
}
else{
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1"/>
  <meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'">
  <title>MSS Attendance</title>
</head>
<body>

 	<div class="row">
	 	<?php include("lib/nav.php"); ?>
 	</div>

  <div class="row">
    <h2 class="center mss">Update Your Password</h2>
  </div>

  <div class="row">
    <?php include("View/profile_password.php"); ?>
  </div>


<?php include("lib/js.php"); ?>
</body>
</html>
<?php  } ?>
